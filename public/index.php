<?php
session_start();
//phpinfo();die;
$directory    = dirname(__DIR__) .DIRECTORY_SEPARATOR.'extensions';
define('dir_extensions',$directory);
/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/Core/functions.php';
require dirname(__DIR__) . '/Core/global.php';

/**
 * Error and Exception handling
 */
error_reporting(-1);
ini_set('error_reporting', E_ALL);
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

$app = new \App\App();
$app->run();