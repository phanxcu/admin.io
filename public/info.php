    <?php
    /**
     *  Template Name: Tư vấn -  Hỏi Đáp */
    ?>
    <?php

    get_header();
    ?>
    <div class="container page-bg_faq" id="wrap-main">
        <div class="row" data-sticky_parent>
            <div class="col-md-8" data-sticky_column>
                <div class="question_as1">
                    <div class="title-faq text-uppercase"> Câu hỏi thường gặp</div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                        <?php
                        $args = array(
                            'post_type' => 'faq',
                            'post_status' => 'publish', 'posts_per_page' => -1,);
                        $stt = 1;
                        $query = new WP_Query($args);
                        if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne<?= $stt ?>">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne<?= $stt ?>" aria-expanded=""
                                           class="<?= $stt == 1 ? ' collapsed' : '' ?>" aria-controls="collapseOne">
                                            <i class="ti-"></i>
                                            <?php the_title(); ?>

                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne<?= $stt ?>"
                                     class="panel-collapse collapse " role="tabpanel"
                                     aria-labelledby="headingOne<?= $stt ?>">
                                    <div class="panel-body">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>

                            <?php $stt++;
                        endwhile; endif; ?>
                        <div id="box-pager" class="pagi text-center"> <?php
                            if (function_exists('wp_simple_pagination')) : ?><?php
                                $option = array(
                                    'text_pages' => '',
                                    'text_first_page' => '', 'text_last_page' => '', 'text_previous_page' => '', 'text_next_page' => '', 'css' => true,
                                    'css_file' => 'default', 'before_pagination' => '<div class="pagination f-Roboto-Regular">', 'after_pagination' => '</div>', 'before_link' => '<li>', 'after_link' => '</li>', 'always_show' => false, 'show_all' => false,
                                    'range' => 3,
                                    'anchor' => 2,
                                );
                                ?>
                                <?php wp_simple_pagination($option) ?>


                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                            <?php wp_reset_postdata(); ?>
                        </div>


                        <?php wp_reset_query();
                        $stt = 1; ?> </div>
                </div>
            </div>
            <div class="col-md-4" id="sidebar" data-sticky_column>
                <div class="form-ask">
                    <img src="<?php echo THEME_DIR; ?>/asset/images/image-faq.png" class="img-responsive img-center"
                         alt="image">
                    <?php echo do_shortcode('[contact-form-7 id="5" title="Đặt câu hỏi"]'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    get_footer(); ?>
    <script type="text/javascript" src="<?= THEME_DIR ?>/asset/js/sticky-kit.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            jQuery("#sidebar").stick_in_parent();
            jQuery('#accordion').collapse({collapsible: true, heightStyle: "content"})
            jQuery('#accordionw').collapse({collapsible: true, heightStyle: "content"})
        });
        //ques-faq
    </script>
