<?php

namespace Core;


/**
 * Base controller
 *
 * PHP version 7.0
 */

abstract class Controller
{

    /**
     * Parameters from the matched route
     * @var array
     */
    protected $route_params = [];
    protected $session;
    protected $limit;
    protected $currentUser;
    protected $db;
    /**
     * Class constructor
     *
     * @param array $route_params  Parameters from the route
     *
     * @return void
     */
    public function __construct($route_params)
    {
        global $user_current;
        $this->route_params = $route_params;
        $this->limit =  (isset($_GET['limit']) && is_numeric($_GET['limit'])) ? $_GET['limit']:10;
        $this->session = new Session();
        $this->setting = new \App\Models\Setting();
        $this->db= new Model();
        $this->currentUser =$user_current;

//        if(isset($route_params['roles'])&& $route_params['roles']=='yes'){
//            if (Session::userIsLoggedIn()) {
//                debug($route_params);
//                $query = $_SERVER['QUERY_STRING'];
//                $query = (substr($query, -1) == '/') ? substr($query, 0, -1) : $query;
//                $currentPage = explode('/',$query);
//                echo $query;
//                $role = $this->currentUser->roles;
//                if(count($currentPage)>1){
//                    $base = ($currentPage[0]);
//                    $action = ($currentPage[1]);
//                    if ( !isset($roles->$base->$action) || $roles->$base->$action !='enable'){
//                        echo  json_response('Bạn không có quyền truy cập!',403);
//                        exit;
//                    }
//                }else{
//                    $base = ($currentPage[0]);
//                    if ( !isset($roles->$base) || $roles->$base !='enable'){
//                        echo  json_response('Bạn không có quyền truy cập!',403);
//                        exit;
//                    }
//                }
//
//            }
//        }


    }
    public function __call($name, $args)
    {
        $method = $name ;

        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array([$this, $method], $args);
                $this->after();
            }
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this));
        }
    }

    /**
     * Before filter - called before an action method.
     *
     * @return void
     */
    protected function before()
    {
    }

    /**
     * After filter - called after an action method.
     *
     * @return void
     */
    protected function after()
    {
    }

}
