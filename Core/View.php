<?php
/**
 * Created by PhpStorm.
 * User: cu
 * Date: 8/8/18
 * Time: 11:59 PM
 */

namespace Core;
use App\Config;
use Core\Cache;
use Core\Csrf;
class View
{
    public static function renderCustom($fileName, $data=[])
    {
        global $lang,$user_current;
        extract($data, EXTR_SKIP);
        try {
            $temp = Config::Template_folder;
            $file = dirname(__DIR__) . "/App/Views/$fileName" .'.php';  // relative to Core directory
            $header = dirname(__DIR__) . "/App/Views/" .'header.php';  // relative to Core directory
            $footer = dirname(__DIR__) . "/App/Views/" .'footer.php';  // relative to Core directory

            if (file_exists($header)) { // include header.php
                require $header;
            } else {
                throw new \Exception('Template ' . $header . ' not found!');
            }
            if (file_exists($file)) {
                require $file;
            } else {
                throw new \Exception('Template ' . $fileName . ' not found!');
            }
            if (file_exists($footer)) { // include footer.php
                require $footer;
            } else {
                throw new \Exception('Template ' . $footer . ' not found!');
            }
        }
        catch (\Exception $e) {
            echo $e->getMessage();
        }

    }
    public static function render($fileName, $data=[])
    {
        global $lang,$user_current;
        extract($data, EXTR_SKIP);
        $html = new HTML();
        try {
            $temp = Config::Template_folder;
            $file = dirname(__DIR__) . "/App/Views/$fileName".'.php';  // relative to Core directory

            if (file_exists($file)) {
                require $file;
            } else {
                throw new \Exception('Template ' . $fileName . ' not found!');
            }

        }
        catch (\Exception $e) {
            echo $e->getMessage();
        }

    }
}