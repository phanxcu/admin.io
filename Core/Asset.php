<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 09-Aug-18
 * Time: 8:19 AM
 */

namespace Core;


class Asset
{
    protected $css = [];
    protected $js = [];
    public function addCss($link){
            $this->css[] = $link;
    }
    public function addJs($link){
            $this->js[] = $link;
    }
    public function getCss()
    {
        return $this->css;
    }
    public function getJs()
    {
        return $this->js;
    }
    public function renderCss(){
        $linkStyleSheet = '';
        foreach ($this->getCss() as $css){
            $css = (strpos('http',$css))?$css:'/'.$css;
            $linkStyleSheet .= '<link href="'.$css.'" rel="stylesheet" type="text/css"/>'.PHP_EOL;
        }
        print  $linkStyleSheet;
    }
    public function renderJs(){
        $linkScript = '';
        // js/owl.carousel.js"
        foreach ($this->getJs() as $css){
            $css = (strpos('http',$css))?$css:'/'.$css;
            $linkScript .= '<script src="'.$css.'" type="text/javascript"></script>'.PHP_EOL;
        }
        print  $linkScript;
    }
}