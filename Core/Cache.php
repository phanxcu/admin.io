<?php
/**
 * Created by PhpStorm.
 * User: cu
 * Date: 8/8/18
 * Time: 10:52 PM
 */

namespace Core;
use App\Config;

class Cache
{
    private $_salt = Config::CacheKey; // <-- change before usage!
    private $_name;
    private $_dir;
    private $_extension;
    private $_path;
    private $_cipher = "aes-128-gcm";

//    public function __construct($name = "default", $dir = "Store/Cache", $extension = ".txt")
//    {
////        if (md5($this->_salt) == "233abed7ee9945c0429047405d864283") throw new Exception("Change _salt value before usage! (line 5)");
//        if ($name == null) throw new Exception("Invalid name argument (empty or null)");
//        if ($dir == null) throw new Exception("Invalid dir argument (empty or null)");
//        if ($extension == null) throw new Exception("Invalid extension argument (empty or null)");
//        $dir = str_replace("\\", "/", $dir);
//        if (!$this->endsWith($dir, "/")) {
//            $dir .= "/";
//        }
//        $this->_name = $name;
//        $this->_dir = $dir;
//        $this->_extension = $extension;
//        $this->_path = $this->getPath();
//        $this->checkDir();
//    }

    public static  function getData($fileName)
    {
        return $fileName;
        $tmp = dirname(__DIR__).DIRECTORY_SEPARATOR."Store/Cache".DIRECTORY_SEPARATOR.$fileName.".txt";
        $cacheFile = @fopen("$tmp", "r");
        if ($cacheFile) {
            $str =  fread($cacheFile, filesize("$tmp"));
            $decodeString = rtrim(openssl_encrypt(MCRYPT_RIJNDAEL_256, md5(Config::CacheKey), base64_decode($str), MCRYPT_MODE_CBC, md5(md5(Config::CacheKey))), "\0");
            return unserialize($decodeString);
            fclose($cacheFile);
        } else {
            return false;
        }
    }

    public function set($key, $value, $ttl = -1)
    {
        $data = [
            "t" => time(),
            "e" => $ttl,
            "v" => serialize($value),
        ];
        $cache = $this->getCache();
        if ($cache == null) {
            $cache = [
                $key => $data,
            ];
        } else {
            $cache[$key] = $data;
        }
        $this->setCache($cache);
    }

    public function get($key, &$out)
    {
        $cache = $this->getCache();
        if (!is_array($cache)) return false;
        if (!array_key_exists($key, $cache)) return false;
        $data = $cache[$key];
        if ($this->isExpired($data)) {
            unset($cache[$key]);
            $this->setCache($cache);
            return false;
        }
        $out = unserialize($data["v"]);
        return true;
    }

    public function remove($key)
    {
        $cache = $this->getCache();
        if (!is_array($cache)) return false;
        if (!array_key_exists($key, $cache)) return false;

        unset($cache[$key]);
        $this->setCache($cache);
        return true;
    }

    private function isExpired($data)
    {
        if ($data["e"] == -1) return false;
        $expiresOn = $data["t"] + $data["e"];
        return $expiresOn < time();
    }

    private function setCache($json)
    {
        if (!is_array($json)) throw new Exception("Invalid cache (not an array?)");
        $content = json_encode($json);
        file_put_contents($this->_path, $content);
    }

    private function getCache()
    {
        if (!file_exists($this->_path)) return null;
        $content = file_get_contents($this->_path);
        return json_decode($content, true);
    }

    private function getPath()
    {
        return $this->_dir . md5($this->_name . $this->_salt) . $this->_extension;
    }

    private function checkDir()
    {
        if (!is_dir($this->_dir) && !mkdir($this->_dir, 0775, true)) {
            throw new Exception("Unable to create cache directory ($this->_dir)");
        }
        if (!is_readable($this->_dir) || !is_writable($this->_dir)) {
            if (!chmod($this->_dir, 0775)) {
                throw new Exception("Store directory must be readable and writable ($this->_dir)");
            }
        }
        return true;
    }

    private function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    private function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return $length === 0 || (substr($haystack, -$length) === $needle);
    }
}