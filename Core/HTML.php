<?php
/**
 * Created by PhpStorm.
 * User: phancu
 * Date: 3/21/2019
 * Time: 9:42 AM
 */

namespace Core;


class HTML
{
    public function select($id, $name, $class, $value, $data = array(), $label, $hide_label = false)
    {

        if (!$hide_label) {
            if ($label != '') echo "<label>$label</label>";
        }
        echo "<select class='$class' id='$id' name='$name'>";
        echo "<option  value=''>$label</option>";
        foreach ($data as $i => $item):
            $selected = $i == $value ? 'selected' : '';
            echo "<option $selected value='$i'>$item</option>";
        endforeach;
        echo "</select>";
    }

    public function inputText($id, $name, $class, $value='',$placeholder='', $label)
    {
        if ($label != '')echo "<label>$label</label>";
        echo "<input class='$class' id='$id' name='$name' type='text' value='$value' placeholder=\"$placeholder\">";
    }
    public function inputRadio($id, $name, $class, $value='',$data, $label)
    {
        $check =  $value == $data?'checked':'';
        echo "<input class='$class' id='$id' name='$name' $check type='radio' value='$value' >";
        if ($label != '')echo "<label for='$id'>$label</label>";

    }
    public function inputTextArea($id, $name, $class, $value='',$placeholder='', $label)
    {
        if ($label != '')echo "<label>$label</label>";
        echo "<textarea  class='$class' id='$id' name='$name'  placeholder=\"$placeholder\" rows='5'>$value</textarea>";
    }
    public function inputEmail($id, $name, $class, $value='',$placeholder='', $label)
    {
        if ($label != '')echo "<label>$label</label>";
        echo "<input class='$class' id='$id' name='$name' type='email' value='$value' placeholder=\"$placeholder\">";
    }
    public function inputNumber($id, $name, $class, $value='',$placeholder='', $label,$min,$max)
    {
        if ($label != '')echo "<label>$label</label>";
        echo "<input class='$class' id='$id' name='$name' type='number' value='$value' min=\"$min\" max=\"$max\" placeholder=\"$placeholder\">";
    }
    public function inputDate($id, $name, $class, $value='', $label)
    {
        if ($label != '')echo "<label>$label</label>";
        echo "<input class='$class' id='$id' name='$name' type='date' value='$value'>";
    }
    public function inputCheckBox($id, $name, $class, $value='', $label)
    {
        $checkeđ= $value!=''?'checked':'';
        echo "<div class=\"checkbox check-info\">
                          <input id=\"$id\" type=\"checkbox\" $checkeđ value=\"yes\">
                          <label for=\"$id\">$label </label>
                        </div>";
    }

}