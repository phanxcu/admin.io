<?php

use Core\Cache;
use App\Config;
use Core\Model;
use App\Models\Role;
use App\Models\User;

function PT_Menu()
{

}

function debug($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function dataByLang($data, $langIn)
{
    global $lang;
    $lng = isset($langIn) ? $langIn : $lang;
    if (is_array($data) || is_object($data)) {
        foreach ($data as $i) {
            if ($i['Languages'] == $lng) {
                return $i;
                break;
            }

        }
    }
    return null;
}

function ResponseJson($code=200,$data = array(),$message=''){
    $dataResponse = [
        'code'=>$code,
        'data'=>$data,
        'message'=>$message
    ];
    return json_encode($dataResponse);
}
function PT_head()
{
    PT_renderCss();

}

function PT_footer()
{
    PT_renderJs();

}

function PT_addCss($css)
{
    global $dataCss;
    $dataCss[] = $css;
}

function PT_addJs($js)
{
    global $dataJs;
    $dataJs[] = $js;
}

function PT_addScriptJs($script)
{
    global $dataScriptJs;
    $dataScriptJs[] = $script;
}

function AZ_addRoute($route = [])
{
    global $route_extensions;
    $route_extensions[] = $route;
}

function PT_renderJs()
{
    $linkScript = '';
    $linkScriptString = '';
    global $dataJs;
    global $dataScriptJs;
    if (is_array($dataJs) || is_object($dataJs)) {
        foreach ($dataJs as $js) {

            if (strpos($js, 'http') !== false) {
                $linkScript .= '<script src="' . $js . '" type="text/javascript"></script>' . PHP_EOL;

            } else {
                $linkScript .= '<script src="' . BASE_URL . '/' . $js . '" type="text/javascript"></script>' . PHP_EOL;

            }
        }
    }
    // js/owl.carousel.js"
    print  $linkScript;
    if (is_array($dataScriptJs) || is_object($dataScriptJs)) {
        foreach ($dataScriptJs as $js) {
            $linkScriptString .= $js . PHP_EOL;
        }
    }
    // js/owl.carousel.js"
    print  $linkScriptString;
}

function PT_renderCss()
{
    global $dataCss;
    $linkStyleSheet = '';
    if (is_array($dataCss) || is_object($dataCss)) {
        foreach ($dataCss as $css) {
            if (strpos($css, 'http') !== false) {
                $linkStyleSheet .= '<link href="' . $css . '" rel="stylesheet" type="text/css"/>' . PHP_EOL;
            } else {
                $linkStyleSheet .= '<link href="' . BASE_URL . '/' . $css . '" rel="stylesheet" type="text/css"/>' . PHP_EOL;


            }

        }
    }
    print  $linkStyleSheet;
}

// Password Security
function passwordGenerator($password)
{
    $salt = uniqid(mt_rand(), true);
    $password .= $salt;
    return password_hash($password, PASSWORD_BCRYPT);
}

function return_error($messenger)
{
    return ['code'=> 400,'data'=>$messenger];
}
function return_success($data)
{
    return ['code'=> 200,'data'=>$data];
}

function redirect($url, $ref)
{
    if (!empty($ref))
        header('location: ' . Config::BaseUrl . '/' . $url . '?redirect=' . urlencode($_SERVER['REQUEST_URI']));
    else
        header('location: ' . Config::BaseUrl . '/' . $url . '/');

}
function json_response($message = null, $code = 200)
{
    // clear the old headers
    header_remove();
    // set the actual code
    http_response_code($code);
    // set the header to make sure cache is forced
    header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
    // treat this as json
    header('Content-Type: application/json');

    $status = array(
        200 => '200 OK',
        400 => '400 Bad Request',
        403 => '403 Not Authentication',
        422 => 'Unprocessable Entity',
        500 => '500 Internal Server Error'
    );
    // ok, validation error, or failure
    header('Status: '.$message);
    // return the encoded json
    return json_encode(array(
        'status' => $code < 300, // success or not?
        'message' => $message
    ));
}
function userData($userData = [])
{
    global $user_current;
    $user_current = new \stdClass();
    foreach ($userData as $i => $val) {
        if($i !='password' && $i!='salt')
        $user_current->$i = $val;
    }
    if (isset($user_current->roleType) && $user_current->roleType=='roles'){
        $dataRoles = new Role();
        $dataRoles->where('_id', new \MongoDB\BSON\ObjectId($user_current->role));
        $roles = $dataRoles->find_one();
        if ($roles){
            $user_current->roles = $roles['roles'];
        }
    }
    $user_current->id = (string)$user_current->_id;
    return $user_current;
}

function getSidebar($name = null)
{
    global $user_current;
    $temp = Config::Template_folder;
    $fileName = $name ? 'sidebar-' . $name : 'sidebar';
    $sidebar = dirname(__DIR__) . "/App/Views/$fileName" . '.php';
    require $sidebar;
}

function getTopbar($name = null)
{
    global $user_current;
    $temp = Config::Template_folder;
    $fileName = $name ? 'topbar-' . $name : 'topbar';
    $Topbar = dirname(__DIR__) . "/App/Views/$fileName" . '.php';
    require $Topbar;
}

function URL($url)
{
    $url = BASE_URL . '/' . $url;
    $url = "javascript:_loadHTML('$url');";
    return $url;
}

function UTCTimeToString($utcdatetime)
{
    $datetime = $utcdatetime->toDateTime();
    $time = $datetime->format(DATE_RSS);
    /********************Convert time local timezone*******************/
    $dateInUTC = $time;
    $time = strtotime($dateInUTC . ' UTC');
    $dateInLocal = date("Y-m-d H:i:s", $time);
    return $dateInLocal;
}

function UTCTimeConvert($time)
{
//    ex: $time = '2016-06-27 13:03:33';
    $orig_date = new \DateTime($time);
    $orig_date = $orig_date->getTimestamp();
    return new \MongoDB\BSON\UTCDateTime($orig_date * 1000);

}
function _lang($string){
    return $string;
}

function getDataExtensions(){
    $db = new Model();

}