<?php

use  Core\Session;
use  App\Config;
date_default_timezone_set(Config::TIME_ZONE);
define('BASE_URL', Config::BaseUrl);
GLOBAL
$dataCss,
$dataStyleCss,
$dataScriptJ,
$dataJs,
$route_current,
$webId,
$lang,
$post_type,
$route_extensions,
$routesSideBar,
$routesUsers,
$user_current;
$lang='en';
if (Session::get('userData')) {
    userData(Session::get('userData')); // fillData to $user_current
}