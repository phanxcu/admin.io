<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 31-Aug-18
 * Time: 10:09 AM
 */

namespace Core;
use App\Config;



class SendMail
{
    public static function send($Subject,$htmlContent,$emails){
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("mail@itppharma.com", "ITP CRM SYSTEM");
        $tos = [
            "phanxcu@gmail.com" => "Example phanxcu",
            "phanch@hyh.com.vn" => "Example phanch",
            "kythuat.itp@gmail.com" => "Example kythuat"
        ];
        $email->addTos($emails);
        $email->setSubject($Subject);
        $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
        $email->addContent("text/html", $htmlContent);
        $sendgrid = new \SendGrid(Config::SEND_GRID_API_KEY);
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
            return true;
        } catch (Exception $e) {
            echo 'Caught exception: '.  $e->getMessage(). "\n";
        }
    }

}