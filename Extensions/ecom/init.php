<?php
return array(
    'version' => '1.0',
    'name' => 'AZ eCommerce',
    'icon' => 'fa-shopping-cart',
    'author' => 'AZCMS',
    'email' => 'AZCMS@gmail.com',
    'website' => 'azcms.com',
    'update' => '09-05-2018',
    'about' => 'eCommerce extensions',
    'url' => '',
    'route' => array(
        "slug" => 'e-commerce',
        "icon" => 'shopping_cart',
        "action" => ['controller' => 'EcommerceController', 'action' => 'dashboard'],
        'name' =>array('en'=>'Shop','vn'=>'Cửa hàng'),
        'children' => [
            array("slug" => "/products", "action" => ['controller' => 'EcommerceController', 'action' => 'products', 'namespace' => 'ecom\controller', 'type' => 'extension'], 'name' => array('vn' => 'Cửa hàng', 'en' => 'Shop'),'show'=>'yes'),
            array("slug" => "/products/add", "action" => ['controller' => 'EcommerceController', 'action' => 'addProduct', 'namespace' => 'ecom\controller', 'type' => 'extension'], 'name' => array('vn' => 'Thêm sản phẩm', 'en' => 'Shop'),'show'=>'yes'),
            array("slug" => "/products/save", "action" => ['controller' => 'EcommerceController', 'action' => 'saveProduct', 'namespace' => 'ecom\controller', 'type' => 'extension'], 'name' => array('vn' => 'Sửa sản phẩm', 'en' => 'Shop'),'show'=>'no'),
            array("slug" => "/products/edit/id:[a-z0-9-]+}", "action" => ['controller' => 'EcommerceController', 'action' => 'editProduct', 'namespace' => 'ecom\controller', 'type' => 'extension'], 'name' => array('vn' => 'Cửa hàng', 'en' => 'Shop'),'show'=>'no'),
            array("slug" => "/products/update/id:[a-z0-9-]+}", "action" => ['controller' => 'EcommerceController', 'action' => 'updateProduct', 'namespace' => 'ecom\controller', 'type' => 'extension'], 'name' => array('vn' => 'Cửa hàng', 'en' => 'Shop'),'show'=>'no'),
            array("slug" => "/products/trash/id:[a-z0-9-]+}", "action" => ['controller' => 'EcommerceController', 'action' => 'trashProduct', 'namespace' => 'ecom\controller', 'type' => 'extension'], 'name' => array('vn' => 'Cửa hàng', 'en' => 'Shop'),'show'=>'no'),
            array("slug" => "/products/delete/id:[a-z0-9-]+}", "action" => ['controller' => 'EcommerceController', 'action' => 'deleteProduct', 'namespace' => 'ecom\controller', 'type' => 'extension'], 'name' => array('vn' => 'Đã xóa', 'en' => 'Shop'),'show'=>'yes'),
        ]
    )
);


?>