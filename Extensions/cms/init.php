<?php
return array(
    'version' => '1.0',
    'name' => 'CMS',
    'icon' => 'fa-shopping-cart',
    'author' => 'CMS',
    'email' => 'phanxcu@gmail.com',
    'website' => 'azcms.com',
    'update' => '09-05-2018',
    'about' => 'CMS extensions',
    'url' => '',
    'route' => array(
        "slug" => 'posts',
        "icon" => 'file_copy',
        "action" => ['controller' => 'PostController', 'action' => 'index'],
        'name' => array('en'=>'Posts','vn'=>'Bài viết'),
        'children' => [
            array("slug" => 'add', "action" => ['controller' => 'PostController', 'action' => 'add'], 'name' => array('en'=>'Add Users','vn'=>'Viết bài mới'), 'menu' => 'yes','roles'=>'yes'),
            array("slug" => 'save', "action" => ['controller' => 'PostController', 'action' => 'save'], 'name' =>array('en'=>'Add Users','vn'=>''), 'menu' => 'no','roles'=>'no'),
            array("slug" => 'edit/{id:[a-z0-9-]+}', "action" => ['controller' => 'PostController', 'action' => 'all'], 'name' =>array('en'=>'Edit Info','vn'=>'Sửa Bài Viết'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'update/{id:[a-z0-9-]+}', "action" => ['controller' => 'PostController', 'action' => 'all'], 'name' =>array('en'=>'Add Users','vn'=>''), 'menu' => 'no','roles'=>'no'),
            array("slug" => 'delete/{id:[a-z0-9-]+}', "action" => ['controller' => 'PostController', 'action' => 'all'], 'name' =>array('en'=>'Add Users','vn'=>'Xóa Bài'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'trash', "action" => ['controller' => 'PostController', 'action' => 'all'], 'name' => array('en'=>'Add Users','vn'=>'Thùng rác'), 'menu' => 'no','roles'=>'yes'),
        ],
    )
);
