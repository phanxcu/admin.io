<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 23-Aug-18
 * Time: 8:29 AM
 */

namespace App\Controllers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Config;
use Core\SendMail;
use Core\View;
use Core\Cache;
use App\Option;
use App\Models\Post;
use Core\Auth;
use Core\Csrf;
use Core\Session;
use Core\Model;
use Core\Pagination;

class FormDataController extends \Core\Controller
{
    public function index()
    {
        $paged = isset($this->route_params['page']) ? $this->route_params['page'] : 1;
        Auth::checkAuthentication();
        PT_addCss('libs/my/daterangepicker.css');
        PT_addCss('libs/my/date-c.css');
        PT_addCss('libs/my/date-c.css');
        PT_addCss('libs/my/switchery.min.css');
        $m = new Model();
        $limit = isset($_GET['limit']) ? $_GET['limit'] : Config::LIMIT;
        $date_search = isset($_GET['date']) && !empty($_GET['date']) ? $_GET['date'] : '';
        $status_s = isset($_GET['status']) && !empty($_GET['status']) ? $_GET['status'] : '';
        $domain_s = isset($_GET['domain']) && !empty($_GET['domain']) ? $_GET['domain'] : '';
        if (!empty($domain_s)) {
            $m->where(['domain' => (string)trim($domain_s)]);
        }
        if (!empty($status_s)) {
            $m->where(['status' => (string)trim($status_s)]);
        }
        $startDate = $endDate = '';
        if (!empty($date_search)) {
            $date = explode(' - ', $date_search);
            $start = strtotime($date[0]);
            $m->where_gt('created_at', (int)$start);

            if ($date[0] == $date[1]) {
                $end = strtotime($date[0] . ' +1 day');
                $m->where_lt('created_at', (int)$end);
            } else {
                $end = strtotime($date[1] . ' +1 day');
                $m->where_lte('created_at', (int)$end);
            }
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1]));

        }
        $offset = $limit * ($paged - 1);
        $m->limit($limit);
        $m->offset($offset);

        $m->order_by(['created_at' => 'ASC']);
        $dataLists = $m->get('formData');
        $pagination = new Pagination(10, $limit, $paged);

//
//      form data search  -----------------------------------------------------
        $db = new Model();
        $listDomain = $db->command(array('distinct' => 'formData', 'key' => 'domain'));
        $listUTM = $db->command(array('distinct' => 'formData', 'key' => 'domain'));
//        debug($listDomain);
//        die;
        $dataRender = [
            'pageTitle' => 'Form Data',
            'metaDescription' => 'Dashboard',
            'metaKeywords' => 'Dashboard',
            'classBody' => 'dashboard-page',
            'listDomain' => $listDomain[0]['values'],
            'dataLists' => $dataLists,
            'pagination' => $pagination,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ];


        View::render('page/formData', $dataRender);
    }

    public function ajax()
    {
        if (isset($_POST['action'])):
            $db = new Model();
            switch ($_POST['action']) {
                case 'changeStatus':
                    $_id = $_POST['_id'];
                    $db->where(["_id" => new \MongoDB\BSON\ObjectId($_id)]);
                    $object = $db->find_one('formData');
                    $status = $object['status'] == 'Pending' ? 'Success' : 'Pending';

                    $db->where(["_id" => new \MongoDB\BSON\ObjectId($_id)]);
                    $db->set('status', $status)->update('formData');
                    debug($db);
                    die;
                    break;
            }
        endif;
    }

    public function putData()
    {

        if ($_POST) {
            $m = new Model();
            $form_type = isset($_POST['form_type']) ? $_POST['form_type'] : 'order';
            $dataForm = null;
            switch ($form_type) {
                case 'advisory': // form tư vấn
                    $dataForm = [
                        'issue' => isset($_POST['issue']) ? $_POST['issue'] : '',
                        'time' => isset($_POST['time']) ? $_POST['time'] : '',
                    ];
                    $dataHTML = "
                    <p> <strong>Issue:</strong> ".isset($_POST['issue']) ? $_POST['issue'] : ''."</p>
                    <p> <strong>Time:</strong> ".isset($_POST['time']) ? $_POST['time'] : ''."</p>
                    ";
                    break;
                case 'order':// mua hàng
                    $dataForm = [
                        'product' => (string)isset($_POST['product']) ? $_POST['product'] : '',
                        'total' => (int)isset($_POST['total']) ? $_POST['total'] : '',
                        'promotion' => (string)isset($_POST['promotion']) ? $_POST['promotion'] : '',

                    ];
                    $dataHTML = "
                    <p> <strong>Product:</strong> ".isset($_POST['product']) ? $_POST['product'] : ''."</p>
                    <p> <strong>Total:</strong> ".isset($_POST['total']) ? $_POST['total'] : ''."</p>
                    <p> <strong>Promotion:</strong> ".isset($_POST['promotion']) ? $_POST['promotion'] : ''."</p>
                    ";
                    break;
            }
            $dataInsert = [
                'fullname' => (string)isset($_POST['fullname']) ? $_POST['fullname'] : '',
                'phone' => (string)isset($_POST['phone']) ? $_POST['phone'] : '',
                'address' => (string)isset($_POST['address']) ? $_POST['address'] : '',
                'note' => (string)isset($_POST['note']) ? $_POST['note'] : '',
                'position' => (string)isset($_POST['position']) ? $_POST['position'] : '',
                'utm_source' => (string)isset($_POST['utm_source']) ? $_POST['utm_source'] : '',
                'utm_medium' => (string)isset($_POST['utm_medium']) ? $_POST['utm_medium'] : '',
                'utm_campaign' => (string)isset($_POST['utm_campaign']) ? $_POST['utm_campaign'] : '',
                'domain' => (string)isset($_POST['domain']) ? $_POST['domain'] : '',
                'dataForm' => $dataForm,
                'url_source' => (string)isset($_POST['url_source']) ? $_POST['url_source'] : '',
                'status' => (string)'Pending',
                'form_type' => (string)$form_type,
                'created_at' => strtotime("+1 day"),
                'updated_at' => time(),
            ];
            $insert = $m->insert('formData', $dataInsert);
//            var_dump($insert);die;
            if ($insert) {
                $date = date('d/m/y');
                $subject = "[$date] THÔNG TIN MUA HÀNG TỪ WEBSITE";
                $emails = [
                    "phanxcu@gmail.com" => "Example phanxcu",
                    "phanch@hyh.com.vn" => "Example phanch",
                    "kythuat.itp@gmail.com" => "Example kythuat"
                ];
                $htmlContent = '
               <h3>THÔNG TIN ĐƠN HÀNG</h3>
<table style="width: 750px;border: 1px solid #ccc" cellpadding="0" cellspacing="0">
    <tr>
        <td style="width: 150px;border: 1px solid #ccc;padding: 5px 10px;">Họ và tên :</td>
        <td style="width: 450px;border: 1px solid #ccc;padding: 5px 10px">'.$_POST['fullname'] ? $_POST['fullname'] :'' .'</td>
    </tr>
    <tr>
        <td style="width: 150px;border: 1px solid #ccc;padding: 5px 10px">Điện thoại :</td>
        <td style="width: 450px;border: 1px solid #ccc;padding: 5px 10px ">'.$_POST['fullname'] ? $_POST['fullname'] :'' .'</td>
    </tr>
    <tr>
        <td style="width: 150px;border: 1px solid #ccc;padding: 5px 10px ">Email :</td>
        <td style="width: 450px;border: 1px solid #ccc;padding: 5px 10px ">'.$_POST['fullname'] ? $_POST['fullname'] :'' .'</td>
    </tr>
    <tr>
        <td style="width: 150px;border: 1px solid #ccc;padding: 5px 10px ">Địa chỉ :</td>
        <td style="width: 450px;border: 1px solid #ccc;padding: 5px 10px ">'.$_POST['fullname'] ? $_POST['fullname'] :'' .'</td>
    </tr>
    <tr>
        <td style="width: 150px;border: 1px solid #ccc;padding: 5px 10px ">Thông tin khác :</td>
        <td style="width: 450px;border: 1px solid #ccc;padding: 5px 10px ">
             '.$dataHTML.'
        </td>
    </tr>
    <tr>
        <td style="width: 150px;border: 1px solid #ccc;padding: 5px 10px ">Đường dẫn chính :</td>
        <td style="width: 450px;border: 1px solid #ccc;padding: 5px 10px ">http://admin.vn</td>
    </tr>
</table> ';
             SendMail::send($subject,$htmlContent,$emails);
            } else {
                echo " false";
            }
        }
    }
    
}