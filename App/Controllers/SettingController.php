<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 23-Aug-18
 * Time: 8:29 AM
 */

namespace App\Controllers;


use App\Config;
use Core\Controller;
use Core\SendMail;
use Core\View;
use Core\Cache;
use App\Option;
use App\Models\Post;
use Core\Auth;
use Core\Csrf;
use Core\Session;
use Core\Model;
use Core\Pagination;
use App\Models\Setting;

class SettingController extends Controller
{
    protected $collection = 'settings';
    protected $template_folder = 'settings';
    protected $setting;
    protected $extensionsConfig;
    public function __construct(array $route_params)
    {
        parent::__construct($route_params);
        $this->setting = new Setting();
    }
    public function general()
    {
        $dataRender = [
            'pageTitle' => $this->route_params['title'],
            'metaDescription' => $this->route_params['title'],
            'metaKeywords' => $this->route_params['title'],
        ];
        View::render($this->template_folder . '/' . $this->route_params['action'], $dataRender);
    }
    public function extensionsUpdate()
    { // Update setting change status
        $this->extensionsConfig = $this->setting->valueSetting('extensions');
        if (isset($_POST['extensionName']) && $_POST['extensionName'] != '') {
            $extensionName = $_POST['extensionName'];
            //        1. if new extentsion
            if (!isset($this->extensionsConfig->$extensionName)) {
                $this->extensionsConfig->$extensionName = 'active';
            } else {//2.if old extension
                $this->extensionsConfig->$extensionName = $this->extensionsConfig->$extensionName == 'active' ? 'inactive' : 'active';
            }
            $update = $this->setting->updateSetting('extensions', $this->extensionsConfig); // update to database
        }

    }

    public function extensions()
    {
        $this->setting = new Setting();
        $extensionsConfig = $this->setting->valueSetting('extensions');
//        Scan Extensions
        $scanned_directory = array_diff(scandir(dir_extensions), array('..', '.'));//array('..', '.') loại bỏ đi những file rác
        $dataExtensions = [];
        foreach ($scanned_directory as $dir) {
            if (!in_array('.', (array)$dir)) {
                $file = dir_extensions . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $dir . ".php";
                if (file_exists($file)) {
                    $dataExtensions[$dir] = require $file;
                    $status = !empty($extensionsConfig->$dir) ? $extensionsConfig->$dir : 'inactive';
                    $dataExtensions[$dir]['status'] = $status;
                }

            }
        }

        $dataRender = [
            'pageTitle' => $this->route_params['title'],
            'metaDescription' => $this->route_params['title'],
            'metaKeywords' => $this->route_params['title'],
            'dataExtensions' => $dataExtensions
        ];
        header('Content-Type: application/json');
        View::render($this->template_folder . '/' . $this->route_params['action'], $dataRender);
    }
}