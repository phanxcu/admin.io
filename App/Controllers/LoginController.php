<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 23-Aug-18
 * Time: 8:28 AM
 */

namespace App\Controllers;

use App\Config;
use App\Models\User;
use Core\View;
use Core\Cache;
use App\Option;
use App\Models\Post;
use Core\Auth;
use Core\Csrf;
use Core\Session;
use Core\Model;

class LoginController extends \Core\Controller
{
    public function logout(){
        session_destroy();
        Auth::redirectLogin();
    }
    public function login()
    {
//        $password = 'huyphan90';
//        $salt = uniqid(mt_rand(), true);
//        $password .= $salt;
//        $password = password_hash($password, PASSWORD_BCRYPT);
//
//        $dataUser['email'] = 'phanxcu@gmail.com';
//        $dataUser['password'] = $password;
//        $dataUser['salt'] = $salt;
//        $dataUser['author_created'] = 1;
//        $dataUser['created_at'] = time();
//        $dataUser['updated_at'] = time();
//        $m = new Model();
//        $insert = $m->insert('az_users', $dataUser); die;
        if (isset($_POST['txtusername']) && $_POST['txtusername']):
            if (!Csrf::isTokenValid()) { // check Csrf
                Auth::redirectLogin();
                echo "sai csrf";
                exit();
            } else {
                $username = $_POST['txtusername'];
                $passs_post = $_POST['txtpassword'];
                $ref =  $_POST['redirect'];
//                get info User from Database
                $User = new User();
                $data = $User->where(['email' => $username])->find_one();
                if($data)// username is correct.
                {
                    $salt1 = $data['salt'];
                    $passs_post .= $salt1;
                    $passsword_correct = $data['password'];

                    $auth = password_verify($passs_post, $passsword_correct);
                    if ($auth) {
                        Session::set('userData',(array)$data);
                        if(!empty($ref)){
                            header('location: ' . Config::BaseUrl. '/');
                        }else{
                            header('location: '. Config::BaseUrl.$ref);
                        }
                        debug($data);

                    } else {
                        // password not correct;
                        Auth::redirectLogin();
                        return_error('sai mật khẩu');
                    }
                }else // username not correct
                {
                    Auth::redirectLogin();
                    return_error

                    ('sai username');
                }
            }
        endif; // end post login;
        $dataRender = [
            'pageTitle' => "Đăng nhập hệ thống",
            'metaDescription' => '',
            'metaKeywords' => '',
            'classBody' => 'login-page'
        ];
        View::render('page/login', $dataRender);

    }

}