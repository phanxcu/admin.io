<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 23-Aug-18
 * Time: 8:29 AM
 */

namespace App\Controllers;

use App\Config;
use App\Models\Setting;
use Core\SendMail;
use Core\View;
use Core\Cache;
use App\Option;
use App\Models\Post;
use Core\Auth;
use Core\Csrf;
use Core\Session;
use Core\Model;
use Core\Pagination;
use App\Models\Role;
use App\Models\User;
use App\Models\Department;
use App\Models\Position;

class UserController extends \Core\Controller
{
    protected $collection = 'users';
    protected $template_folder = 'users';
    protected $slugBase = 'users';
    protected $db;
    protected $user;
    protected $Role;
    protected $department;
    protected $position;

    function __construct($route_params)
    {
        parent::__construct($route_params);
        $this->setting = new Setting();
        $this->user = new User();
        $this->Role = new Role();
        $this->department = new Department();
        $this->position = new Position();

    }

    public function index()
    {
        Auth::checkAuthentication();
        $paged = isset($_GET['paged']) ? $_GET['paged'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : 'created_at';
        $order = isset($_GET['order']) ? $_GET['order'] : 'DESC';
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $status = isset($_GET['search']) ? $_GET['status'] : '';
        $offset = $this->limit * ($paged - 1);
        if (!empty($search))
            $this->user->where_or(array(
                    'email' => new \MongoDB\BSON\Regex('^' . $search, 'i'),
                    'fullName' => new \MongoDB\BSON\Regex('^' . $search, 'i')
                )
            );
        $total = $this->user->count();
        $this->user->limit($this->limit);
        $this->user->offset($offset);
        $this->user->order_by([$order_by => $order]);
        if (!empty($search))
            $this->user->where_or(array(
                    'email' => new \MongoDB\BSON\Regex('^' . $search, 'i'),
                    'fullName' => new \MongoDB\BSON\Regex('^' . $search, 'i')
                )
            );
        $dataLists = $this->user->get();
        $pagination = new Pagination($total, $this->limit, $paged, '');
        $dataRender = [
            'search' => $search,
            'limit' => $limit,
            'status' => $status,
            'order_by' => $order_by,
            'order' => $order,
            'pageTitle' => 'Danh thành viên',
            'metaDescription' => 'Danh sách quyền',
            'metaKeywords' => 'Dashboard',
            'classBody' => 'dashboard-page',
            'paged' => $paged,
            'dataLists' => $dataLists,
            'pagination' => $pagination,
            'slugBase' => $this->slugBase,
        ];
        View::render($this->template_folder . '/index', $dataRender);
    }

    public function department()
    {
        Auth::checkAuthentication();
        $paged = isset($_GET['paged']) ? $_GET['paged'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : 'created_at';
        $order = isset($_GET['order']) ? $_GET['order'] : 'DESC';
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $status = isset($_GET['search']) ? $_GET['status'] : '';
        $offset = $this->limit * ($paged - 1);
        if (!empty($search))
            $this->department->where_or(array(
                    'department_name' => new \MongoDB\BSON\Regex('^' . $search, 'i'),
                    'department_short_name' => new \MongoDB\BSON\Regex('^' . $search, 'i')
                )
            );
        $total = $this->department->count();
        $this->department->limit($this->limit);
        $this->department->offset($offset);
        $this->department->order_by([$order_by => $order]);
        if (!empty($search))
            $this->department->where_or(array(
                    'department_name' => new \MongoDB\BSON\Regex('^' . $search, 'i'),
                    'department_short_name' => new \MongoDB\BSON\Regex('^' . $search, 'i')
                )
            );
        $dataLists = $this->department->get();
        $pagination = new Pagination($total, $this->limit, $paged, '');
        $dataRender = [
            'search' => $search,
            'limit' => $limit,
            'status' => $status,
            'order_by' => $order_by,
            'order' => $order,
            'pageTitle' => 'PHÒNG BAN',
            'metaDescription' => '',
            'metaKeywords' => 'Dashboard',
            'classBody' => 'dashboard-page',
            'paged' => $paged,
            'dataLists' => $dataLists,
            'pagination' => $pagination,
            'slugBase' => $this->slugBase . '/department',

        ];

        View::render($this->template_folder . '/department/index', $dataRender);
    }

    public function departmentAdd() // user
    {
        if (isset($_POST['action']) && $_POST['action'] == 'add') {
            $dataDepartment = $_POST;
            unset($dataDepartment['action']);
            $dataDepartment['author_created'] = $this->currentUser->id;
            $dataDepartment['created_at'] = time();
            $dataDepartment['updated_at'] = time();
            $this->department->add($dataDepartment);
            echo ResponseJson(200, $dataDepartment['department_name'], "Thêm thành công");
            exit();
        }
        $dataRender = [
            'pageTitle' => 'Thêm mới phòng ban',
            'metaDescription' => 'Thêm mới người dùng',
            'metaKeywords' => 'Add New User',
            'classBody' => 'add-page',
            'slugBase' => $this->slugBase,
        ];
        header('Content-Type: application/json');
        View::render($this->template_folder . '/department/add', $dataRender);

    }

    public function departmentEdit() // user
    {
        $id = $this->route_params['id'];
        if (isset($_POST['action']) && $_POST['action'] == 'edit') {
            $dataEdit['author_updated'] = $this->currentUser->id;
            $dataEdit = array_merge($_POST, $dataEdit);
            unset($dataEdit['action']);
            $dataEdit['updated_at'] = time();
            $this->department->where('_id', new \MongoDB\BSON\ObjectId($id));
            $this->department->set($dataEdit);
            $updated = $this->department->update($dataEdit);
            if (is_array($updated) && isset($updated['code'])) {
                echo ResponseJson($updated['code'], $updated, $updated['message']);
            } else {
                echo ResponseJson(200, $updated, 'Sửa thành công!');
            }
            exit();

        }

        $dataParent = $this->department->where('status', 'Enabled')->where('parent', '')->get();
        $this->department->where('_id', new \MongoDB\BSON\ObjectId($id));
        $dataEdit = $this->department->find_one();
        $dataRender = [
            'pageTitle' => '',
            'metaDescription' => '',
            'metaKeywords' => 'Add New User',
            'classBody' => 'add-page',
            'slugBase' => $this->slugBase . '/department',
            'dataEdit' => $dataEdit,
            'dataParent' => $dataParent,

        ];
        header('Content-Type: application/json');
        View::render($this->template_folder . '/department/edit', $dataRender);

    }

    public function position()
    {
        Auth::checkAuthentication();
        $dataDepartments = $this->department->where('status', 'Enabled')->get();

        $listSelectDepartment = array();
        foreach ($dataDepartments as $item) {
            $id = (string)$item['_id'];
            $listSelectDepartment[$id] = $item['department_name'];
        }
        $paged = isset($_GET['paged']) ? $_GET['paged'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : 'created_at';
        $order = isset($_GET['order']) ? $_GET['order'] : 'DESC';
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $status = isset($_GET['search']) ? $_GET['status'] : '';
        $offset = $this->limit * ($paged - 1);
        if (!empty($search))
            $this->position->where_or(array(
                    'position_name' => new \MongoDB\BSON\Regex('^' . $search, 'i'),
                    'position_short_name' => new \MongoDB\BSON\Regex('^' . $search, 'i')
                )
            );
        $total = $this->position->count();
        $this->position->limit($this->limit);
        $this->position->offset($offset);
        $this->position->order_by([$order_by => $order]);
        if (!empty($search))
            $this->position->where_or(array(
                    'position_name' => new \MongoDB\BSON\Regex('^' . $search, 'i'),
                    'position_short_name' => new \MongoDB\BSON\Regex('^' . $search, 'i')
                )
            );
        $dataLists = $this->position->get();
        $pagination = new Pagination($total, $this->limit, $paged, '');
        $dataRender = [
            'search' => $search,
            'limit' => $limit,
            'status' => $status,
            'order_by' => $order_by,
            'order' => $order,
            'pageTitle' => 'CHỨC VỤ',
            'metaDescription' => '',
            'metaKeywords' => 'Dashboard',
            'classBody' => 'dashboard-page',
            'paged' => $paged,
            'dataLists' => $dataLists,
            'pagination' => $pagination,
            'listSelectDepartment' => $listSelectDepartment,
            'slugBase' => $this->slugBase . '/position',

        ];

        View::render($this->template_folder . '/position/index', $dataRender);
    }

    public function positionAdd() // user
    {
        if (isset($_POST['action']) && $_POST['action'] == 'add') {
            $dataAdd = $_POST;
            unset($dataAdd['action']);
            $dataAdd['author_created'] = $this->currentUser->id;
            $dataAdd['created_at'] = time();
            $dataAdd['updated_at'] = time();
            $this->position->add($dataAdd);
            echo ResponseJson(200, $dataAdd['position_name'], "Thêm thành công");
            exit();
        }
        $dataDepartments = $this->department->where('status', 'Enabled')->get();
        $dataPosition = $this->position->where('status', 'Enabled')->get();

        $listSelectDepartment = array();
        foreach ($dataDepartments as $item) {
            $id = (string)$item['_id'];
            $listSelectDepartment[$id] = $item['department_name'];
        }
        $listdataPosition = array();
        foreach ($dataPosition as $item) {
            $id = (string)$item['_id'];
            $listdataPosition[$id] = $item['position_name'];
        }
        $dataRender = [
            'pageTitle' => 'Thêm mới phòng ban',
            'metaDescription' => 'Thêm mới người dùng',
            'metaKeywords' => 'Add New User',
            'classBody' => 'add-page',
            'dataDepartments' => $listSelectDepartment,
            'dataPosition' => $listdataPosition,
            'slugBase' => $this->slugBase,
        ];
        header('Content-Type: application/json');
        View::render($this->template_folder . '/position/add', $dataRender);

    }

    public function positionEdit() // user
    {
        $_id = $this->route_params['id'];
        if (isset($_POST['action']) && $_POST['action'] == 'edit') {
            $dataEdit['author_updated'] = $this->currentUser->id;
            $dataEdit = array_merge($_POST, $dataEdit);
            unset($dataEdit['action']);
            $dataEdit['updated_at'] = time();
            $this->position->where('_id', new \MongoDB\BSON\ObjectId($_id));
            $this->position->set($dataEdit);
            $updated = $this->position->update($dataEdit);
            if (is_array($updated) && isset($updated['code'])) {
                echo ResponseJson($updated['code'], $updated, $updated['message']);
            } else {
                echo ResponseJson(200, $updated, 'Sửa thành công!');
            }
            exit();

        }
        $dataDepartments = $this->department->where('status', 'Enabled')->get();
        $dataPosition = $this->position->where('status', 'Enabled')->get();

        $listSelectDepartment = array();
        foreach ($dataDepartments as $item) {
            $id = (string)$item['_id'];
            $listSelectDepartment[$id] = $item['department_name'];
        }
        $listdataPosition = array();
        foreach ($dataPosition as $item) {
            $id = (string)$item['_id'];
            $listdataPosition[$id] = $item['position_name'];
        }
        $this->position->where('_id', new \MongoDB\BSON\ObjectId($_id));
        $dataEdit = $this->position->find_one();
        $dataRender = [
            'pageTitle' => '',
            'metaDescription' => '',
            'metaKeywords' => 'Add New User',
            'classBody' => 'add-page',
            'slugBase' => $this->slugBase . '/position',
            'dataEdit' => $dataEdit,
            'dataDepartments' => $listSelectDepartment,
            'dataPosition' => $listdataPosition,

        ];
        header('Content-Type: application/json');
        View::render($this->template_folder . '/position/edit', $dataRender);
    }
    public function add() // user
    {
        if (isset($_POST['action']) && $_POST['action'] == 'add') {
            $dataUser = $_POST;
            $password = $_POST['txt_password'];
            $salt = uniqid(mt_rand(), true);
            $password .= $salt;
            $password = password_hash($password, PASSWORD_BCRYPT);
            unset($dataUser['action']);
            unset($dataUser['password_repeat']);
            unset($dataUser['txt_password']);
            $dataUser['password'] = $password;
            $dataUser['salt'] = $salt;
            $dataUser['author_created'] = $this->currentUser->id;
            $dataUser['created_at'] = time();
            $dataUser['updated_at'] = time();
            $id = $this->user->add($dataUser);
            echo ResponseJson(200, $dataUser['fullName'], "Thêm thành công");
            exit();
        }
        $roles = new Role();
        $dataRoles = $roles->where('status', 'Enabled')->get();
        $dataRender = [
            'pageTitle' => 'Thêm mới người dùng',
            'metaDescription' => 'Thêm mới người dùng',
            'metaKeywords' => 'Add New User',
            'classBody' => 'add-page',
            'dataRoles' => $dataRoles,
            'slugBase' => $this->slugBase,
        ];
        header('Content-Type: application/json');
        View::render($this->template_folder . '/' . __FUNCTION__, $dataRender);

    }
    public function update()
    {
        $id = $this->route_params['id'];
        $dataExtra = [
            'updated_at' => time(),
        ];
        if ($_POST) {
            $dateUpdated = array_merge($_POST, $dataExtra);
            unset($dateUpdated['email']);
            $this->user->where('_id', new \MongoDB\BSON\ObjectId($id));
            $this->user->set($dateUpdated);
            $updated = $this->user->update($dateUpdated);
            if (is_array($updated) && isset($updated['code'])) {
                echo ResponseJson($updated['code'], $updated, $updated['message']);
            } else {
                echo ResponseJson(200, $updated, 'Sửa thành công!');
            }
        } else   echo ResponseJson(400, ['Lỗi'], "Lỗi trong quá trình gửi thông tin");
        exit();
    }
    public function delete()
    {
        $id = $this->route_params['id'];
        $idFind = array('_id' => new \MongoDB\BSON\ObjectId($id));
        $deleted = $this->user->delete($idFind);
        if (is_array($deleted) && isset($updated['code'])) {
            echo ResponseJson($deleted['code'], $deleted, $deleted['message']);
        } else {
            echo ResponseJson(200, $deleted, 'Xóa thành công!');
        }
        exit();
    }
    public function resetPassword()
    {
        if (isset($_POST['id']) && $_POST['id'] != '') {
            $id = $_POST['id'];
            $password = $_POST['password'];
            $salt = uniqid(mt_rand(), true);
            $password .= $salt;
            $password = password_hash($password, PASSWORD_BCRYPT);
            $dataUser['password'] = $password;
            $dataUser['salt'] = $salt;
            $dataUser['author_created'] = $this->currentUser->id;
            $dataUser['updated_at'] = time();
            $this->user->where('_id', new \MongoDB\BSON\ObjectId($id));
            $this->user->set($dataUser);
            $updated = $this->user->update($this->collection, $dataUser);
            if (is_array($updated) && isset($updated['code'])) {
                echo ResponseJson($updated['code'], $updated, $updated['message']);
            } else {
                echo ResponseJson(200, $updated, 'Sửa thành công!');
            }
        } else   echo ResponseJson(400, ['Lỗi'], "Lỗi trong quá trình gửi thông tin");
        exit();
    }
    public function edit() // user
    {
        $id = $this->route_params['id'];
        $roles = new Role();
        $dataRoles = $roles->where('status', 'Enabled')->get();
        $this->user->where('_id', new \MongoDB\BSON\ObjectId($id));
        $dataEdit = $this->user->find_one();
        $dataRender = [
            'pageTitle' => '',
            'metaDescription' => '',
            'metaKeywords' => 'Add New User',
            'classBody' => 'add-page',
            'slugBase' => $this->slugBase,
            'dataEdit' => $dataEdit,
            'dataRoles' => $dataRoles,

        ];
        header('Content-Type: application/json');
        View::render($this->template_folder . '/' . __FUNCTION__, $dataRender);

    }
    public function checkEmailUnique()
    {
        if (isset($_POST['email']) && $_POST['email'] != '') {
            $dataUsers = $this->user->get();
            foreach ($dataUsers as $user) {
                if ($_POST['email'] == $user['email']) {
                    die ("false");
                    break;
                }
            }
            die('true');
        } else die ("false");
    }

}