<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 16-Oct-18
 * Time: 12:07 PM
 */

namespace App\Controllers;

use Core\Controller;
use App\Models\Setting;
use App\Models\Role;
use Core\View;
use Core\Pagination;
use Core\Auth;

class RoleController extends Controller
{
    protected $template_folder = 'roles';
    protected $role;

    function __construct($route_params)
    {
        parent::__construct($route_params);
        $this->role = new Role();
        $this->limit = 10;

    }

    public function index()
    {
        $paged = isset($this->route_params['page']) ? $this->route_params['page'] : 1;
        Auth::checkAuthentication();
        $total = $this->role->count();
        $offset = $this->limit * ($paged - 1);
        $this->role->limit($this->limit);
        $this->role->offset($offset);
        $this->role->order_by(['created_at' => 'DESC']);
        $dataLists = $this->role->get();

        $pagination = new Pagination($total, $this->limit, $paged, '');
        $dataRender = [
            'pageTitle' => 'Danh sách quyền',
            'metaDescription' => 'Danh sách quyền',
            'metaKeywords' => 'Dashboard',
            'classBody' => 'dashboard-page',
            'dataLists' => $dataLists,
            'paged' => $paged,
            'pagination' => $pagination,
        ];


        View::render($this->template_folder . '/'.__FUNCTION__, $dataRender);
    }

    public function add() // user
    {
        if(isset($_POST['action']) && $_POST['action'] == 'add'){
            $dataExtra = [
                'author_created' => $this->currentUser->id,
                'created_at' => time(),
                'updated_at' => time(),
            ];
            $id = $this->role->add(array_merge($_POST, $dataExtra));
            echo ResponseJson(200, $id, "Thêm thành công");
            exit();
        }
        $dataRender = [
            'pageTitle' => 'Thêm quền',
            'metaDescription' => 'Add New User',
            'metaKeywords' => 'Add New User',
            'classBody' => 'add-page',
        ];
        header('Content-Type: application/json');
        View::render($this->template_folder . '/'.__FUNCTION__, $dataRender);

    }

    public function delete()
    {
        $id = $this->route_params['id'];

        $idFind = array('_id' => new \MongoDB\BSON\ObjectId($id));
        $deleted = $this->role->delete($idFind);
        if (is_array($deleted) && isset($deleted['code'])) {
            echo ResponseJson($deleted['code'], $deleted, $deleted['message']);
        } else {
            echo ResponseJson(200, $deleted, 'Xóa thành công!');
        }
        exit();
    }
    public function edit() // user
    {
        $id = $this->route_params['id'];
        if(isset($_POST['action']) && $_POST['action'] == 'edit') { // check post_data
            $dataExtra = [
                'updated_at' => time(),
            ];
            if ($_POST['action']) {
                $dateUpdated = array_merge($_POST, $dataExtra);
                $this->role->findID($id);
                $this->role->set($dateUpdated);
                $updated = $this->role->update($dateUpdated);
                if (is_array($updated) && isset($updated['code'])) {
                    echo ResponseJson($updated['code'], $updated, $updated['message']);
                } else {
                    echo ResponseJson(200, $updated, 'Sửa thành công!');
                }
            } else   echo ResponseJson(400, ['Lỗi'], "Lỗi trong quá trình gửi thông tin");
            exit();
        }
        $this->role->findID($id);
        $dataEdit = $this->role->find_one();
        $dataRender = [
            'pageTitle' => '',
            'metaDescription' => '',
            'metaKeywords' => 'Add New User',
            'classBody' => 'add-page',
            'dataEdit' => $dataEdit
        ];
        header('Content-Type: application/json');
        View::render($this->template_folder . '/'.__FUNCTION__, $dataRender);

    }

}