<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 09-Aug-18
 * Time: 9:48 AM
 */
namespace App\Controllers;
use App\Config;
use Core\View;
use Core\Cache;
use App\Option;
use App\Models\Post;
use Core\Auth;
use Core\Csrf;
class PageController extends \Core\Controller
{
    protected $Option ;
    public function __construct(array $route_params)
    {
        $this->Option =  new Option();
    }

    public function homeProcess(){

        $DataSetting = Cache::getData('setting'); // Thông tin cài đặt trang web
        $lang = $DataSetting['Languages']; // Ngôn ngữ Mặc định
        $webId = $DataSetting['_id']; // ID Web
        $dataHome = dataByLang($DataSetting['home'],$lang);
//        debug($dataHome);die;
//        PT_addCss('css/style.css');
//        $a = Post::getPost('ddđ');
//        $Languages =  Cache::getCacheData('Languages');
//        debug($this->Option->Languages);


      $dataRender = [
          'pageTitle'=>$dataHome['name'],
          'metaDescription'=>$dataHome['des_seo'],
          'metaKeywords'=>$dataHome['key_seo'],
          'classBody'=>'home-page'
      ];
      View::render('page/home',$dataRender);

    }

    public function home(){
        Auth::checkAuthentication();
        $dataRender = [
            'pageTitle'=>'Dashboard',
            'metaDescription'=>'Dashboard',
            'metaKeywords'=>'Dashboard',
            'classBody'=>'dashboard-page'
        ];
        View::renderCustom('base',$dataRender);

    }
    public function page(){
        $slug = $this->route_params['slug'];
        // Check HomePage /vi/ Multi Languages

        if (Config::multiLang && in_array($slug,Config::Lang)):// Kiểm tra xem có là đa ngôn ngữ hay ko
            $this->homeProcess();

        else: // Not Multi Language
           echo "Page category";
        endif;
    }
    public function pageNotFound(){

        echo "404 Page";
    }
}