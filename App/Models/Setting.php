<?php
namespace App\Models;

use App\Config;

class Setting extends \Core\Model
{


    public function __construct()
    {
        parent::__construct();
        $this->collection = Config::DB_PREFIX .'settings';

    }
    public function addSetting($key,$value){
        $this->insert( array($key=>$value));
    }
    public function valueSetting($key){
        $value = $this->where(array('key' => (string)$key))->find_one();
        if(!empty($value) && isset($value['value']))
        return $value['value'];

    }
    public function updateSetting($key,$value){
        $updated = $this->where(array('key' => (string)$key))->set('value', $value)->update();
        return $updated;
    }
}