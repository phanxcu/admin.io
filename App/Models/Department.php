<?php
/**
 * Created by PhpStorm.
 * User: phancu
 * Date: 3/8/2019
 * Time: 2:36 PM
 */

namespace App\Models;
use Core\Model;
class Department extends Model
{

    public function __construct()
    {
        parent::__construct();
        $this->collection =  \App\Config::DB_PREFIX.'departments';

    }
    public function add($data=[]){
        $this->insert( $data);
    }
    public function getByID($id){
        $value = $this->where(array('_id' => new \MongoDB\BSON\ObjectId($id)))->find_one();
        if(!empty($value) && isset($value['value']))
            return $value['value'];
        return null;
    }
    public function updateSetting($key,$value){
        $updated = $this->where(array('key' => (string)$key))->set('value', $value)->update();
        return $updated;
    }
}