<?php

namespace App\Models;


/**
 * Example user model
 *
 * PHP version 7.0
 */
class User  extends \Core\Model
{

    public function __construct()
    {
        parent::__construct();
        $this->collection = \App\Config::DB_PREFIX.'users';

    }
    public function add($data=[]){
        $this->insert( $data);
    }
    public function getByID($id){
        $value = $this->where(array('_id' => new \MongoDB\BSON\ObjectId($id)))->find_one();
        if(!empty($value) && isset($value['value']))
            return $value['value'];
        return null;
    }
    public function updateSetting($key,$value){
        $updated = $this->where(array('key' => (string)$key))->set('value', $value)->update();
        return $updated;
    }
}