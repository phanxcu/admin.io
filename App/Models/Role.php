<?php
namespace App\Models;
use App\Config;

class Role extends \Core\Model
{

    public $name;
    public function __construct()
    {
        parent::__construct();
        $this->name= 'roles';
        $this->collection = Config::DB_PREFIX . $this->name;

    }
    public function add($data=[]){
        $this->insert($data);
    }

    public function valueSetting($key){
        $value = $this->where(array('key' => (string)$key))->find_one();
        if(!empty($value) && isset($value['value']))
            return $value['value'];

    }
    public function updateSetting($key,$value){
        $updated = $this->where(array('key' => (string)$key))->set('value', $value)->update();
        return $updated;
    }
}