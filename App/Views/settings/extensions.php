<div class="content sm-gutter">
    <div class="page-title">
        <h3><?= $pageTitle ?></h3>
    </div>
    <!-- BEGIN DASHBOARD TILES -->
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple ">
                <div class="grid-title no-border">

                </div>
                <div class="grid-body no-border">
                    <!--                        <h3>Stripped <span class="semi-bold">Table</span></h3>-->
                    <br>
                    <table class="table table-striped table-flip-scroll cf">
                        <thead class="cf alert-success">
                        <tr>
                            <th>
                                <div class="checkbox check-default ">
                                    <input id="checkbox1" type="checkbox" value="1" class="checkall">
                                    <label for="checkbox1"></label>
                                </div>
                            </th>
                            <th>Extensions</th>
                            <th>Infomation</th>
                            <th>status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $ios = '';
                        foreach ($dataExtensions as $i => $item):

                            $ios .= " 
                                var changeCheckbox$i = document.querySelector('#iosblue$i');
                                console.log(changeCheckbox$i);
                                var xxx$i = new Switchery(changeCheckbox$i);
                                changeCheckbox$i.onchange = function() {
                                  changeStatus('$i');
                                };";
                            ?>
                            <tr>
                                <td>
                                    <div class="checkbox check-default">
                                        <input id="checkbox<?= $i ?>" type="checkbox" value="1">
                                        <label for="checkbox<?= $i ?>"></label>
                                    </div>
                                </td>
                                <td>
                                    <?= $item['name'] ?> <br>
                                </td>

                                <td>
                                   <p> <?= $item['about'] ?> </p>
                                    <p>Version : <b> <?=$item['version']?></b> | By : <b><?=$item['version']?></b> | Website : <a href="<?=$item['website']?>"><b><?=$item['website']?></b></a></p>
                                </td>

                                <td>
                                    <div class="slide-primary">
                                        <input type="checkbox" name="switch<?= $i ?>" class="ios" id="iosblue<?= $i ?>" <?= $item['status'] == 'inactive' ? '' : 'checked' ?> />
                                    </div>

                                </td>

                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    
                    <div class="col-md-11">
                        <div class="pagination">
                            <?php //echo $pagination->toHtml(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD TILES -->

</div>
<script type="text/javascript">
    $(document).ready(function () {
        <?=$ios;?>
    });
    function changeStatus(_id) {

        $.ajax({
            url: 'settings/extensions/update',
            type: 'post',
            data: {extensionName:_id,action:'changeStatus'},
            success: function( data, textStatus, jQxhr ){
                console.log(data);
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });

    }
</script>
<?php


?>


