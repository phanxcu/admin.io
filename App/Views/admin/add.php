<div class="content sm-gutter">
    <div class="page-title">
        <h3><?= $pageTitle ?></h3>
    </div>
    <ul class="breadcrumb">
        <li><a href="<?= URL('/') ?>">Home </a></li>
        <li><a href="<?= URL('users') ?>">Users </a></li>
        <li><a class="active" href="<?= URL('users/add') ?>">Add User </a></li>
    </ul>
    <!-- BEGIN DASHBOARD TILES -->
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple form-grid">
                <div class="grid-body no-border">
                    <form class="form-no-horizontal-spacing form_validation" id="form-condensed">
                        <div class="row column-seperation">
                            <div class="col-md-6">
                                <h4>Basic Information</h4>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <input name="email" id="email" type="text" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input name="password" id="password" type="password" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="col-md-6">
                                        <input name="password_repeat" id="password-re" type="password" class="form-control" placeholder="Re-Password">
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input name="form3FirstName" id="form3FirstName" type="text" class="form-control" placeholder="First Name">
                                    </div>
                                    <div class="col-md-6">
                                        <input name="form3LastName" id="form3LastName" type="text" class="form-control" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <select name="form3Gender" id="form3Gender" class="select2 form-control">
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Date of Birth" class="form-control" id="birth-day" name="birth-day">
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input name="text" id="address" type="text" class="form-control" placeholder="Address">
                                    </div>
                                    <div class="col-md-6">
                                        <input name="text" id="phone" type="text" class="form-control" placeholder="Phone">
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-6">
                                <h4>Role</h4>

                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input name="form3City" id="form3City" type="text" class="form-control" placeholder="City">
                                    </div>
                                    <div class="col-md-6">
                                        <input name="form3State" id="form3State" type="text" class="form-control" placeholder="State">
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-8">
                                        <input name="form3Country" id="form3Country" type="text" class="form-control" placeholder="Country">
                                    </div>
                                    <div class="col-md-4">
                                        <input name="form3PostalCode" id="form3PostalCode" type="text" class="form-control" placeholder="Postal Code">
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <input name="form3TeleCode" id="form3TeleCode" type="text" class="form-control" placeholder="+94">
                                    </div>
                                    <div class="col-md-8">
                                        <input name="form3TeleNo" id="form3TeleNo" type="text" class="form-control" placeholder="Phone Number">
                                    </div>
                                </div>
                                <div class="row small-text">
                                    <p class="col-md-12">
                                        NOTE - Facts to be considered, Simply remove or edit this as for what you desire. Disabled font Color and size
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="pull-left">
                                <div class="checkbox checkbox check-success 	">
                                    <input type="checkbox" value="1" id="chkTerms">
                                    <label for="chkTerms">I Here by agree on the Term and condition. </label>
                                </div>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-primary  btn-cons" type="submit"><i class="icon-ok"></i> Save</button>
                                <button class="btn btn-white btn-cons" type="button">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD TILES -->

</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(function ($) {
            $("#birth-day").mask("99/99/9999");
        });

        //form validation rules
        $("#form-condensed").validate({
            rules:
                {
                    email:
                        {
                            required: true,
                            email: true,
                            remote: {
                                url: "users/email",
                                type: "post",
                                data: {
                                    email: function () {
                                        return $("#email").val();
                                    }
                                }
                            },
                        },
                    password:
                        {
                            required: true,
                            minlength: 8
                        },
                    password_repeat:
                        {
                            required: true,
                            equalTo: password,
                            minlength: 8
                        }
                },
            messages:
                {
                    email:
                        {
                            required: "Please enter your email address.",
                            email: "Please enter a valid email address.",
                            remote: jQuery.validator.format("{0} is already taken.")
                        },
                    password: "Please enter a password.",
                    password_repeat: "Passwords must match."
                },
            invalidHandler: function (event, validator) {
                //display error alert on form submit
            },

            errorPlacement: function (label, element) { // render error placement for each input type
                $('<span class="error"></span>').insertAfter(element).append(label)
            },

            highlight: function (element) { // hightlight error inputs

            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {

            },

            submitHandler: function (form) {

            }
        });


    });

</script>


