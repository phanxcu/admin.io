
<div class="page-content " id="htmlAjax">
    <div class="content sm-gutter">
        <div class="page-title">
            <h3><?= $pageTitle ?></h3>
        </div>
        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">

        </div>
        <!-- END DASHBOARD TILES -->

    </div>
</div>
<!-- END PAGE CONTAINER -->

<style>
    .modal-lg {
        width: 80%;
    }
</style>
<!-- END CONTAINER -->
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<!-- BEGIN JS DEPENDECENCIES-->
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-block-ui/jqueryblockui.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>

<script src="<?=\App\Config::BaseUrl?>/libs/my/switchery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.3/underscore-min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/0.9.10/backbone-min.js"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-notifications/js/messenger.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-notifications/js/messenger-theme-future.js" type="text/javascript"></script>

<!--<script type="text/javascript" src="--><?//=\App\Config::BaseUrl?><!--/assets/plugins/jquery-notifications/js/demo/location-sel.js"></script>-->
<!--<script type="text/javascript" src="--><?//=\App\Config::BaseUrl?><!--/assets/plugins/jquery-notifications/js/demo/theme-sel.js"></script>-->
<?php ob_start(); ?>
<script type="text/javascript">

    //_sortOrderBy
    function _sortOrderBy(field,order) {
        document.getElementById('order_by').value= field;
        document.getElementById('order').value= order;
        searchForm();

    }

    function showMessage(type,title,message){
        $('#messageContent').removeClass('alert-success alert-warring alert-info alert-error');
        $('#messageContent').addClass('alert-'+type);
        $('#messageContent h4').text(title);
        $('#messageContent h5').text(message);
        $('#messageModal').modal('show');
    }

    function changeStatus(_id) {
        $.ajax({
            url: '/ajax-data',
            type: 'post',

            data: {_id: _id, action: 'changeStatus'},
            success: function (data, textStatus, jQxhr) {
                console.log(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });

    }
    function deleteDataForm(url,page) {
        $('#actionDelete').val(url);
        $('#page').val(page);
        $('#deleteDataModal').modal({show:true});
    }
    function deleteDataFormSubmit() {
        $('#deleteDataModal').modal('hide');
        var page = $('#page').val();
        $.ajax({
            url: $('#actionDelete').val(),
            type: 'get',
            dataType: 'html',
            success: function (data, textStatus, jQxhr) {
                showMessage('Delete','Xóa thành công','')
                _loadHTML(page);
                console.log(data);

            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
    function limitChange(v) {
        document.getElementById('hideLimit').value = v;
        searchForm();
    }
    // Function js Route ajax Loading HTML
    function _loadHTML(url) {
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'html',
            success: function (data, textStatus, jQxhr) {
                $('#htmlAjax').html(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    // Function js add New Modal ajax Loading HTML
    function addNew(url) {
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'html',
            success: function (data, textStatus, jQxhr) {
                $('#ajaxBodyAddNewModalHTML').html(data);
                $('#addNewModal').modal({show:true});
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }// Function js saveData ajax Loading HTML

    function saveData() {
        $form = $('#ajaxBodyAddNewModalHTML form').submit();

    }

    // Function js add New Modal ajax Loading HTML
    function searchForm(url,page) {
        var actionUrl = $('#searchForm').attr('action');
        $.ajax({
            url: actionUrl,
            type: 'get',
            data:$('#searchForm').serializeArray(),
            dataType: 'html',
            success: function (data, textStatus, jQxhr) {
                $('#htmlAjax').html(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
    function editData(url,page) {
        $('#page').val(page);
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'html',
            success: function (data, textStatus, jQxhr) {
                // console.log(data);
                // console.log(textStatus);
                // console.log(jQxhr);
                $('#ajaxBodyUpdateModalHTML').html(data);
                $('#editDataModal').modal({show:true});
            },
            error: function (jqXhr, textStatus, errorThrown) {
                res = (JSON.parse(jqXhr.responseText));
                showMessage('error','Cảnh báo',res.message);
            }
        });
    }// Function js saveData ajax Loading HTML

    function pagination (page) {
        document.getElementById('page_s').value = page;
        searchForm();
    }
</script>

<!-- Modal Add New -->
<div class="modal fade" id="addNewModal" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="addNewModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                <h2 class="semi-bold">NHẬP THÔNG TIN</h2>
            </div>
            <div class="modal-body" id="ajaxBodyAddNewModalHTML">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger " data-dismiss="modal">HỦY KHÔNG LƯU</button>
                <button type="button" class="btn btn-primary" onclick="saveData()">LƯU LẠI</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal Add New -->
<!-- Modal Edit Data -->
<div class="modal fade" id="editDataModal" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="editDataModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                <h2 class="semi-bold">SỬA THÔNG TIN</h2>
            </div>
            <div class="modal-body" id="ajaxBodyUpdateModalHTML">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger " data-dismiss="modal">HỦY</button>
                <button type="button" class="btn btn-primary" onclick="$('#ajaxBodyUpdateModalHTML form').submit()">LƯU LẠI</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal Add New -->
<!-- Modal Edit Data -->
<div class="modal fade" id="deleteDataModal" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="editDataModal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body text-center" id="">
                <div class="alert alert-block alert-error fade in" style="margin-bottom: 0">
                    <h4 class="alert-heading"><i class="icon-warning-sign"></i> Cảnh báo!</h4>
                    <h5>  <a href="#" class="link"></a> Bạn chắn chắn xóa dữ liệu đã chọn ? </h5>
                    <div class="button-set">
                        <button class="btn btn-danger btn-cons" type="button"  onclick="deleteDataFormSubmit()" >Xóa</button>
                        <button class="btn btn-white btn-cons" type="button"data-dismiss="modal" >Hủy</button>
                    </div>
                </div>
                <form action="" id="formDelete">
                    <input type="hidden" name="action" id="actionDelete">
                    <input type="hidden" name="id" id="actionID">

                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="messageModal" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="messageModal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body text-center" id="">
                <div id="messageContent" class="alert alert-block alert-error fade in" style="margin-bottom: 0">
                    <h4 class="alert-heading"><span>Cảnh báo!</span> </h4>
                    <h5> Bạn chắn chắn xóa dữ liệu đã chọn ? </h5>
                    <div class="button-set">
                        <button class="btn btn-warning btn-cons" type="button"data-dismiss="modal" >Đóng</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<input type="hidden" name="page" id="page">
<?php
$js = ob_get_contents();
ob_clean();
ob_end_flush();
PT_addScriptJs($js);
