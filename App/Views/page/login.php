<?php
use App\Config;
use Core\Csrf;
?>
<!DOCTYPE HTML>
<html lang="<?php echo $lang; ?>" itemscope itemtype="http://schema.org/WebSite">
<head>
    <meta charset="utf-8">
    <title><?php echo $pageTitle; ?></title>
    <link href="<?=BASE_URL?>/assets/img/logo-az.png" rel="shortcut icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="Cache-control" content="public">
    <link rel="canonical" href="<?php if (isset($ogUrl)) {
        echo $ogUrl;
    } ?>"/>
    <meta name="description" content="<?php echo $metaDescription; ?>"/>
    <meta name="keywords" content="<?php echo $metaKeywords; ?>"/>
    <meta property="og:title" content="<?php if (isset($pageTitle)) {
        echo $pageTitle;
    } ?>"/>
    <meta property="og:url" content="<?php if (isset($ogUrl)) {
        echo $ogUrl;
    } ?>"/>
    <meta property="og:image" content="<?php if (isset($ogImage)) {
        echo $ogImage;
    } ?>"/>
    <meta property="og:type" content="text/html"/>
    <meta name="twitter:card" content="summary"/>
    <?php
    PT_head();
    ?>
    <!-- BEGIN PLUGIN CSS -->
    <link href="<?=BASE_URL?>/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?=BASE_URL?>/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=BASE_URL?>/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?=BASE_URL?>/assets/plugins/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=BASE_URL?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- END PLUGIN CSS -->
    <link href="<?=BASE_URL?>/libs/css/webarch.css" rel="stylesheet" type="text/css" />
</head>


<body id="PT<?php echo $classBody; ?>"
      itemscope itemtype="http://schema.org/WebPage" class=" <?php echo $classBody; ?> error-body no-top  pace-done"
      data-content="content-v1">
<div class="container">
    <div class="row login-container column-seperation">
        <div class="col-md-5 col-md-offset-1 text-center">
            <img style="max-width: 100%;width: 300px;margin-bottom: 20px;" src="<?=BASE_URL?>/assets/img/logo.png" alt="">
            <h2>
                Hệ thống HYH Group
            </h2>
            <p>
                Quản lý nhân sự, công việc,và ... tất cả
            </p>
            <br>
        </div>
        <div class="col-md-5 ">
            <div class="row">
                <div class="form-group col-md-offset-1 col-md-10">
            <h3>ĐĂNG NHẬP HỆ THỐNG</h3>
                </div>
                </div>
                <form action="" class="login-form validate" id="login-form" method="post" name="login-form">
                <div class="row">
                    <div class="form-group col-md-offset-1 col-md-10">
                        <input type="hidden" name="csrf_token" value="<?= Csrf::makeToken(); ?>" />
                        <input type="hidden" name="redirect" value="<?= isset($_GET['redirect'])?$_GET['redirect']:'' ?>" />
                        <label class="form-label">Tài khoản</label>
                        <input class="form-control" id="txtusername" name="txtusername" type="text" required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-10 col-md-offset-1">
                        <label class="form-label">Mật khẩu</label> <span class="help"></span>
                        <input class="form-control" id="txtpassword" name="txtpassword" type="password" required>
                    </div>
                </div>
                <div class="row">
                    <div class="control-group col-md-10 col-md-offset-1">
                        <div class="checkbox checkbox check-success">
<!--                            <a href="#">Trouble login in?</a>&nbsp;&nbsp;-->
<!--                            <input id="checkbox1" name="remember-login" type="checkbox" value="1">-->
<!--                            <label for="checkbox1">Ghi nhớ đăng nhập</label>-->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 offset1">
                        <button class="btn btn-primary btn-cons pull-right" type="submit">Đăng nhập</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTAINER -->
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<!-- BEGIN JS DEPENDECENCIES-->
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-block-ui/jqueryblockui.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<!-- END CORE JS DEPENDECENCIES-->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="<?=\App\Config::BaseUrl?>/libs/js/webarch.js" type="text/javascript"></script>
<script src="<?=\App\Config::BaseUrl?>/assets/js/chat.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS -->
<?php
PT_footer();
?>
</body>
</html>