<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content sm-gutter">
        <div class="page-title">
            <h3><?= $pageTitle ?></h3>
        </div>
        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                    <div class="grid-title no-border">
                        <form action="" method="get">
                            <div class="row">
                                <div class="col-md-2">
                                    <label class="semi-bold" for="">Times</label>
                                    <div id="reportrange"
                                         style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span> <i class="fa fa-caret-down"></i>
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <label class="semi-bold" for="">Domain</label>
                                    <select class="form-control" name="domain">
                                        <?php
                                        debug($listDomain);
                                        ?>
                                        <?php foreach ($listDomain as $item): ?>
                                            <option value="<?=$item?>" <?=$item==isset($_GET['domain']) && $_GET['domain']==$item?'selected':''?>><?=$item?></option>
                                        <?php endforeach; ?>

                                    </select>

                                </div>
                                <div class="col-md-2">

                                    <label class="semi-bold" for="">UTM</label>
                                    <div id="reportrange"
                                         style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span> <i class="fa fa-caret-down"></i>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="semi-bold" for="">Status</label>
                                    <div id="reportrange"
                                         style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span> <i class="fa fa-caret-down"></i>
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <label class="semi-bold" for="">Lọc</label>
                                    <button type="button" class="btn btn-primary btn-cons"><i class="fa fa-filter"></i> &nbsp;Filter</button>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="grid-body no-border">
                        <h3>Stripped <span class="semi-bold">Table</span></h3>
                        <br>
                        <table class="table table-striped table-flip-scroll cf">
                            <thead class="cf alert-success">
                            <tr>
                                <th>
                                    <div class="checkbox check-default ">
                                        <input id="checkbox1" type="checkbox" value="1" class="checkall">
                                        <label for="checkbox1"></label>
                                    </div>
                                </th>
                                <th>Fullname</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Note</th>
                                <th>total</th>
                                <th>button_click</th>
                                <th>utm_source</th>
                                <th>utm_medium</th>
                                <th>utm_campaign</th>
                                <th>domain</th>
                                <th>status</th>
                                <th>status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($dataLists as $i=>$item): ?>
                            <tr>
                                <td>
                                    <div class="checkbox check-default">
                                        <input id="checkbox<?=$item['_id']?>" type="checkbox" value="1">
                                        <label for="checkbox<?=$item['_id']?>"></label>
                                    </div>
                                </td>
                                <td><?=$item['fullname']?></td>
                                <td><?=$item['phone']?></td>
                                <td><?=$item['address']?></td>
                                <td><?=$item['note']?></td>
                                <td><?=$item['position']?></td>
                                <td><?=$item['utm_source']?></td>
                                <td><?=$item['utm_medium']?></td>
                                <td><?=$item['utm_campaign']?></td>
                                <td><?=$item['domain']?></td>
                                <td><?=$item['status']?></td>
                                <td><?=$item['form_type']?></td>
                                <td><span class="label label-important">ALERT!</span>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <div class="pagination">
                        <?php echo  $pagination->toHtml(); ?>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILES -->

    </div>
</div>
<!-- END PAGE CONTAINER -->
<?php
$js = "
<script type=\"text/javascript\">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>
";
PT_addJs('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js');
PT_addJs('https://cdnjs.cloudflare.com/ajax/libs/trianglify/0.2.1/trianglify.min.js');
PT_addJs('libs/my/daterangepicker.js');
PT_addJs('libs/my/date-c.js');
PT_addScriptJs($js);
?>


