<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content sm-gutter">
        <div class="page-title">
            <h3><?= $pageTitle ?></h3>
        </div>
        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                    <div class="grid-title no-border">
                        <form action="" method="get" id="searchForm">
                            <div class="row">
                                <div class="col-md-2">
                                    <div id="dateRanger">
                                        <input id="reportrange" name="date" class="form-control" readonly style="cursor: pointer" value="<?=isset($_GET['date'])?$_GET['date']:''?>"/>
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <i class="fa fa-caret-down"></i>
                                    </div>
                                    <input type="hidden" name="limit" value="" id="hideLimit">


                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="domain">
                                        <option value="">Domain</option>
                                        <?php foreach ($listDomain as $item): ?>
                                            <option value="<?= $item ?>" <?= $item == isset($_GET['domain']) && $_GET['domain'] == $item ? 'selected' : '' ?>><?= $item ?></option>
                                        <?php endforeach; ?>

                                    </select>

                                </div>
                                <div class="col-md-2">

                                    <select class="form-control" name="type">
                                        <option value="">FORM TYPE</option>
                                        <option value="order" <?= $item == isset($_GET['type']) && $_GET['type'] == 'order' ? 'order' : '' ?>>Order</option>
                                        <option value="advisory" <?= $item == isset($_GET['type']) && $_GET['type'] == 'advisory' ? 'advisory' : '' ?>>Advisory</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="status">
                                        <option value="">Status</option>
                                        <option value="Pending" <?= $item == isset($_GET['status']) && $_GET['status'] == 'Pending' ? 'selected' : '' ?>>Pending</option>
                                        <option value="Success" <?= $item == isset($_GET['status']) && $_GET['status'] == 'Success' ? 'selected' : '' ?>>Success</option>
                                    </select>

                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary btn-cons"><i class="fa fa-filter"></i> &nbsp;Filter</button>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="grid-body no-border">
<!--                        <h3>Stripped <span class="semi-bold">Table</span></h3>-->
                        <br>
                        <table class="table table-striped table-flip-scroll cf">
                            <thead class="cf alert-success">
                            <tr>
                                <th>
                                    <div class="checkbox check-default ">
                                        <input id="checkbox1" type="checkbox" value="1" class="checkall">
                                        <label for="checkbox1"></label>
                                    </div>
                                </th>
                                <th>Time</th>
                                <th>Fullname</th>
                                <th>infomation</th>
                                <th>Position</th>
                                <th>utm</th>
                                <th>domain</th>
                                <th>status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $ios=''; foreach ($dataLists as $i => $item):
                                $_id=$item['_id'];
                                $ios .=" 
                                var changeCheckbox$i = document.querySelector('#iosblue$i');
                                console.log(changeCheckbox$i);
                                var xxx$i = new Switchery(changeCheckbox$i);
                                changeCheckbox$i.onchange = function() {
                                  changeStatus('$_id');
                                };";
                                ?>
                                <tr>
                                    <td>
                                        <div class="checkbox check-default">
                                            <input id="checkbox<?= $item['_id'] ?>" type="checkbox" value="1">
                                            <label for="checkbox<?= $item['_id'] ?>"></label>
                                        </div>
                                    </td>
                                    <td><?= date('d/m/Y',$item['created_at']) ?></td>
                                    <td>
                                        <?= $item['fullname'] ?> <br>
                                        <?= $item['phone'] ?> <br>
                                        <?= $item['address'] ?> <br>
                                    </td>

                                    <td>
                                    <?php if($item['form_type'] == 'order'): ?>
                                        <label>Order:</label>
                                        <b>Product:</b><?= $item['dataForm']->product ?> <br>
                                        <b>Total:</b><?= $item['dataForm']->total ?> <br>
<!--                                        <b>Price:</b>--><?//= $item['total'] ?><!-- <br>-->
                                    <?php else: ?>
                                        <label>Advisory:</label>
                                        <b>Time:</b><?= $item['dataForm']->time ?> <br>
                                        <b>Issue:</b><?= $item['dataForm']->issue ?> <br>
                                    <?php endif; ?>
                                    </td>
                                    <td><?= $item['position'] ?></td>
                                    <td>
                                        <b>Source :</b> <?= $item['utm_source'] ?><br>
                                        <b>Medium :</b><?= $item['utm_medium'] ?><br>
                                        <b>Campaign :</b><?= $item['utm_campaign'] ?>
                                    </td>
                                    <td><?= $item['domain'] ?></td>
                                    <td>
                                        <div class="slide-primary">
                                            <input type="checkbox" name="switch<?=$i?>" class="ios"  id="iosblue<?=$i?>" <?=$item['status']=='Pending'?'':'checked'?> />
                                        </div>
                                        <?=$item['status']?>
                                    </td>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="col-md-1">
                            <select class="form-control" id="limit" onchange="limitChange(this.value);">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </div>
                        <div class="col-md-11">
                            <div class="pagination">
                                <?php echo $pagination->toHtml(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILES -->

    </div>
</div>
<!-- END PAGE CONTAINER -->
<?php
$startDate = $startDate!=''?"'".$startDate."'":'';
$endDate = $endDate!=''?"'".$endDate."'":'';


echo $ios;
$js = "
<script type=\"text/javascript\">
function changeStatus(_id) {
 
  $.ajax({
                url: '/ajax-data',
                type: 'post',
                data: {_id:_id,action:'changeStatus'},
                success: function( data, textStatus, jQxhr ){
                      console.log(data);
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });

}
function limitChange(val){
    document.getElementById('hideLimit').value= val;
    document.getElementById('searchForm').submit();
}
$(function() {
    
    var start = moment($startDate);
    var end = moment($endDate);

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'All': ,
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
      
    }, cb);

    cb(start, end);
    $ios
});
</script>
";
PT_addJs('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js');
PT_addJs('https://cdnjs.cloudflare.com/ajax/libs/trianglify/0.2.1/trianglify.min.js');
PT_addJs('libs/my/daterangepicker.js');
PT_addJs('libs/my/date-c.js');
PT_addJs('libs/my/switchery.min.js');
PT_addJs('libs/my/script.js');
PT_addScriptJs($js);
?>


