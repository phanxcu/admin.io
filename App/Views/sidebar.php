<div class="page-sidebar " id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
<!--        <div class="user-info-wrapper sm">-->
<!--            <div class="profile-wrapper sm">-->
<!--                <img src="--><?//= BASE_URL ?><!--/assets/img/profiles/avatar.jpg" alt="" data-src="--><?//= BASE_URL ?><!--/assets/img/profiles/avatar.jpg"-->
<!--                     data-src-retina="--><?//= BASE_URL ?><!--/assets/img/profiles/avatar2x.jpg" width="69" height="69"/>-->
<!--                <div class="availability-bubble online"></div>-->
<!--            </div>-->
<!--            <div class="user-info sm">-->
<!--                <div class="username">--><?php //echo($user_current->username); ?><!--</div>-->
<!--                <div class="status"></div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <p class="menu-title sm">BROWSE <span class="pull-right"><a href="javascript:;"><i class="material-icons">refresh</i></a></span></p>-->
        <ul>
            <li class="start active "><a href="<?= URL('') ?>"><i class="material-icons">dashboard</i> <span class="title">Dashboard</span> <span class="selected"></span> </a></li>
            <!-- END MINI-PROFILE -->
            <!-- BEGIN SIDEBAR MENU -->
            <?php
            global $routesSideBar;
            foreach ($routesSideBar as $menu):
                $hasChild = count($menu['children']) > 0 ? true : false;
                ?>
                <li>
                    <a href="<?= $hasChild ? 'javascript:;' : URL($menu['slug']) ?>">
                        <i class="material-icons"><?= isset($menu['icon']) ? $menu['icon'] : '' ?></i>
                        <span class="title"><?= $menu['name'][\App\Config::LangDefault] ?></span>
                        <?= $hasChild ? '<span class=" arrow"></span>' : '' ?>
                    </a>
                    <?php if ($hasChild): ?>
                        <ul class="sub-menu">
                            <li><a href="<?= URL($menu['slug']) ?>"> <?= $menu['name'][\App\Config::LangDefault] ?></a></li>
                            <?php foreach ($menu['children'] as $child): ?>
                                <?php if (isset($child['menu'])&& $child['menu']=='yes'): ?>
                                    <li><a href="<?= URL($menu['slug'] . '/' . $child['slug']) ?>"> <?= $child['name'][\App\Config::LangDefault] ?></a></li>
                                <?php endif; ?>

                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>

        </ul>

        <div class="clearfix"></div>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
