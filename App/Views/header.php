<?php
use App\Config;
?>
<!DOCTYPE HTML>
<html lang="<?php echo $lang; ?>" itemscope itemtype="http://schema.org/WebSite">
<head>
    <meta charset="utf-8">
    <title><?php echo $pageTitle; ?></title>
    <link href="<?=BASE_URL?>/assets/img/logo-az.png" rel="shortcut icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="Cache-control" content="public">
    <link rel="canonical" href="<?php if (isset($ogUrl)) {
        echo $ogUrl;
    } ?>"/>
    <meta name="description" content="<?php echo $metaDescription; ?>"/>
    <meta name="keywords" content="<?php echo $metaKeywords; ?>"/>
    <meta property="og:title" content="<?php if (isset($pageTitle)) {
        echo $pageTitle;
    } ?>"/>
    <meta property="og:url" content="<?php if (isset($ogUrl)) {
        echo $ogUrl;
    } ?>"/>
    <meta property="og:image" content="<?php if (isset($ogImage)) {
        echo $ogImage;
    } ?>"/>
    <meta property="og:type" content="text/html"/>
    <meta name="twitter:card" content="summary"/>

    <!-- BEGIN PLUGIN CSS -->
    <link href="<?=BASE_URL?>/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?=BASE_URL?>/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?=BASE_URL?>/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=BASE_URL?>/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?=BASE_URL?>/assets/plugins/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=BASE_URL?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" />
    <link href="<?=BASE_URL?>/libs/my/switchery.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=BASE_URL?>/assets/plugins/jquery-notifications/css/messenger.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?=BASE_URL?>/assets/plugins/jquery-notifications/css/messenger-theme-flat.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?=BASE_URL?>/assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />

    <?php
    PT_head();
    ?>
    <!-- END PLUGIN CSS -->
    <link href="<?=BASE_URL?>/libs/css/webarch.css" rel="stylesheet" type="text/css" />
    <style>
       .btn-material span {
           position: relative;
           bottom: 4px;
       }
       .btn-material i {
            display: inline-block;
            position: relative;
            top: 4px;
        }

    </style>
</head>
<body class="horizontal-menu  pace-done <?=isset($classBody)?$classBody:''?>">
<!-- BEGIN HEADER -->
<?php getTopbar(); ?>
<!-- END HEADER -->
<!-- BEGIN CONTENT -->
<div class="page-container row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php //getSidebar(); ?>
    <!-- END SIDEBAR -->
