<div class="row">
    <div class="col-md-12">
        <div class="grid simple form-grid">
            <div class="grid-body no-border">
                <form class="form-no-horizontal-spacing form_validation" id="form-condensed"
                      action="/roles/edit/<?= $dataEdit['_id'] ?>">
                    <input type="hidden" name="action" value="edit">

                    <div class="row column-seperation">
                        <div class="col-md-4">
                            <h4>Thông tin</h4>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <input name="name" id="name" type="text" class="form-control"
                                           placeholder="Tên vai trò" value="<?= $dataEdit['name'] ?>">
                                </div>
                            </div>

                            <div class="row form-row">
                                <div class="col-md-12">
                                    <textarea id="text-editor" name="description" placeholder="Mô tả vai trò ..."
                                              class="form-control" rows="10"><?= $dataEdit['description'] ?></textarea>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <label for=""><strong>Trạng thái</strong></label>
                                    <div class="radio radio-primary">
                                        <input id="Disabled" type="radio" name="status"
                                               value="disabled" <?= $dataEdit['status'] == 'disabled' ? 'checked="checked"' : '' ?>>
                                        <label for="Disabled">Disabled</label>
                                        <input id="ADisabled" type="radio" name="status"
                                               value="Enabled" <?= $dataEdit['status'] == 'Enabled' ? 'checked="checked"' : '' ?>>
                                        <label for="ADisabled">Enabled</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <style>
                                .material-icons.role-icon {
                                    font-size: 19px;
                                    position: relative;
                                    top: 3px;
                                    margin-right: 5px;
                                    text-shadow: none;
                                }
                            </style>
                            <h4>Quyền</h4>
                            <?php
                            $dataRole = $dataEdit['role'];
                            global $routesSideBar;
                            foreach ($routesSideBar as $menu):
                                $hasChild = count($menu['children']) > 0 ? true : false;
                                ?>
                                <div class=" form-row">
                                    <fieldset>
                                        <legend><label>
                                                <strong>
                                                    <i class="material-icons role-icon"><?= isset($menu['icon']) ? $menu['icon'] : '' ?></i>
                                                    <span class="title"><?= $menu['name'][\App\Config::LangDefault] ?></span>
                                                </strong>
                                            </label></legend>
                                        <div class=" form-row">

                                            <?php if ($hasChild):
                                                foreach ($menu['children'] as $child):
                                                    if (isset($child['roles']) && $child['roles'] == 'yes'):
                                                    $object = $menu['slug'];
                                                    $action = $child['action']['action'];
                                                    $check = isset($dataEdit['role']->$object->$action) &&  $dataEdit['role']->$object->$action == 'enable'?'checked':'';
                                                    ?>
                                                    <div class="col-md-3">
                                                        <div class="checkbox check-primary">
                                                            <input class="all_checkbox<?= $object ?>"
                                                                   name="role[<?= $object ?>][<?= $action ?>]"
                                                                   id="checkbox<?= $object . '/' . $child['slug'] ?>"
                                                                   <?=$check?>
                                                                   type="checkbox" value="enable">
                                                            <label for="checkbox<?= $object . '/' . $child['slug'] ?>"> <?= $child['name'][\App\Config::LangDefault] ?></label>
                                                        </div>
                                                    </div>
                                                    <?php endif;
                                                endforeach;
                                                endif; ?>
                                        </div>
                                    </fieldset>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".checkALLRule").change(function () {  //"select all" change
            var status = this.checked; // "select all" checked status
            var id = $(this).attr('id');
            $('.' + id).each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });


    });
    $("#form-condensed").validate({

        submitHandler: function (form) {
            var actionUrl = $(form).attr('action');
            $.ajax({
                url: actionUrl,
                type: 'post',
                data: $(form).serializeArray(),
                dataType: 'html',
                beforeSend: function () {
                },
                success: function (data, textStatus, jQxhr) {
                    $('#editDataModal').modal('hide');
                    showMessage('success', '', 'Cập nhật thành công!');
                    _loadHTML($('#page').val())
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });

        }
    });

</script>
