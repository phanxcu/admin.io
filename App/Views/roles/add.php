<div class="row">
    <div class="col-md-12">
        <div class="grid simple form-grid">
            <div class="grid-body no-border">
                <form class="form-no-horizontal-spacing form_validation" id="form-condensed" action="/roles/add">
                    <input type="hidden" name="action" value="add">
                    <div class="row column-seperation">
                        <div class="col-md-4">
                            <h4>Thông tin</h4>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <input name="name" id="name" type="text" class="form-control" placeholder="Tên vai trò">
                                </div>
                            </div>

                            <div class="row form-row">
                                <div class="col-md-12">
                                    <textarea id="text-editor" name="description" placeholder="Mô tả vai trò ..." class="form-control" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <label for=""><strong>Trạng thái</strong></label>
                                    <div class="radio radio-primary">
                                        <input id="Disabled" type="radio" name="status" value="disabled">
                                        <label for="Disabled">Disabled</label>
                                        <input id="ADisabled" type="radio" name="status" value="Enabled" checked="checked">
                                        <label for="ADisabled">Enabled</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <style>
                                .material-icons.role-icon {
                                    font-size: 19px;
                                    position: relative;
                                    top: 3px;
                                    margin-right: 5px;
                                    text-shadow: none;
                                }
                            </style>
                            <h4>Quyền</h4>
                            <?php
                            global $routesSideBar;
                            foreach ($routesSideBar as $menu):
                                $hasChild = count($menu['children']) > 0 ? true : false;
                                ?>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend><label>
                                                    <strong>
                                                        <i class="material-icons role-icon"><?= isset($menu['icon']) ? $menu['icon'] : '' ?></i>
                                                        <span class="title"><?= $menu['name'][\App\Config::LangDefault] ?></span>
                                                    </strong>
                                                </label></legend>


                                            <?php if ($hasChild): ?>
                                                <?php foreach ($menu['children'] as $child): ?>
                                                    <?php if (isset($child['roles']) && $child['roles'] == 'yes'): ?>
                                                        <div class="col-md-3">
                                                            <div class="checkbox check-primary">
                                                                <input class="all_checkbox<?= $menu['slug'] ?>" name="role[<?= $menu['slug'] ?>][<?= $child['action']['action'] ?>]"
                                                                       id="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>" type="checkbox" value="enable">
                                                                <label for="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>"> <?= $child['name'][\App\Config::LangDefault] ?></label>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </fieldset>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".checkALLRule").change(function () {  //"select all" change
            var status = this.checked; // "select all" checked status
            var id = $(this).attr('id');
            $('.' + id).each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        //form validation rules
        $("#form-condensed").validate({

            invalidHandler: function (event, validator) {
                //display error alert on form submit
            },

            errorPlacement: function (label, element) { // render error placement for each input type
                $('<span class="error"></span>').insertAfter(element).append(label)
            },

            highlight: function (element) { // hightlight error inputs

            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {

            },

            submitHandler: function (form) {
                var actionUrl = $(form).attr('action');
                $.ajax({
                    url: actionUrl,
                    type: 'post',
                    data:$(form).serializeArray(),
                    dataType: 'html',
                    success: function (data, textStatus, jQxhr) {
                        $('#addNewModal').modal('hide');
                        showMessage('success', '', 'Tạo thành công!');
                        _loadHTML('/roles');
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });

            }
        });
    })

</script>
