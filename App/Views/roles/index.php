<!-- BEGIN PAGE CONTAINER-->
<div class="content sm-gutter">
        <div class="page-title">
            <h3><?= $pageTitle ?></h3>
        </div>
        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                    <div class="grid-title no-border text-right">
                        <button onclick="addNew('/roles/add')" type="button" class="btn btn-primary btn-cons btn-material"><i class="material-icons">add_circle_outline</i> <span>Thêm mới</span></button>
                    </div>
                    <div class="grid-body no-border">
                        <table class="table table-striped table-flip-scroll cf">
                            <colgroup>
                                <col style="width:2%">
                                <col style="width:10%">
                                <col style="width:30%">
                                <col style="width:10%">
                                <col style="width:5%">
                                <col style="width:5%">
                            </colgroup>
                            <thead class="cf label-inverse">
                            <tr>
                                <th>
                                    <div class="checkbox check-default ">
                                        <input id="checkbox1" type="checkbox" value="1" class="checkall">
                                        <label for="checkbox1"></label>
                                    </div>
                                </th>
                                <th>Vai trò</th>
                                <th>Mô tả</th>
                                <th>Ngày tạo</th>
                                <th>Trạng thái</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $ios=''; foreach ($dataLists as $i => $item):
                                $_id=$item['_id'];
                                $ios .=" 
                                var changeCheckbox$i = document.querySelector('#iosblue$i');
                                var xxx$i = new Switchery(changeCheckbox$i);
                                changeCheckbox$i.onchange = function() {

                                };";
                                ?>
                                <tr id="<?= $item['_id'] ?>">
                                    <td>
                                        <div class="checkbox check-default">
                                            <input id="checkbox<?= $item['_id'] ?>" type="checkbox" value="1">
                                            <label for="checkbox<?= $item['_id'] ?>"></label>
                                        </div>
                                    </td>

                                    <td>
                                        <?= $item['name'] ?>
                                    </td>
                                    <td><?= $item['description'] ?></td>
                                    <td><?= date('d/m/Y',$item['created_at']) ?></td>
                                    <td>
                                        <div class="slide-primary">
                                            <input type="checkbox" name="switch<?=$i?>" class="ios"  id="iosblue<?=$i?>" <?=$item['status']!='Enabled'?'':'checked'?> />
                                        </div>
                                    </td>
                                    <td>
                                        <button class="btn btn-primary btn-xs btn-mini btn-info inline tip" data-toggle="tooltip" title="" data-original-title="Sửa!" onclick="editData('roles/edit/<?= $item['_id'] ?>','roles/page/<?=$paged?>')"> <i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-primary btn-xs btn-mini btn-danger inline tip"data-toggle="tooltip" title="" data-original-title="Xóa!" onclick="deleteDataForm('roles/delete/<?= $item['_id'] ?>','roles/page/<?=$paged?>')"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="col-md-1">
                            <select class="form-control" id="limit" onchange="limitChange(this.value);">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </div>
                        <div class="col-md-11">
                            <div class="pagination">
                                <?php echo $pagination->toHtml(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILES -->

    </div>
<!-- END PAGE CONTAINER -->
<script>
    <?=$ios;?>
</script>
<?php
PT_addJs('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js');
PT_addJs('https://cdnjs.cloudflare.com/ajax/libs/trianglify/0.2.1/trianglify.min.js');
PT_addJs('libs/my/daterangepicker.js');
PT_addJs('libs/my/date-c.js');
PT_addJs('libs/my/switchery.min.js');
PT_addJs('libs/my/script.js');

?>


