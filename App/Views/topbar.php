<div class="header navbar navbar-inverse ">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <!-- END RESPONSIVE MENU TOGGLER -->
        <div class="header-quick-nav">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="pull-left">
                <div class="logo">
                    <a href="#" class="" id="layout-condensed-toggle">
                        <img src="<?=BASE_URL?>/assets/img/logo.png" class="" alt="" data-src="<?=BASE_URL?>/assets/img/logo_w.png" data-src-retina="<?=BASE_URL?>/assets/img/logo2x.png" width="130" height="auto" />

                    </a>
                </div>
            </div>
            <!-- END TOP NAVIGATION MENU -->
            <!-- BEGIN CHAT TOGGLER -->
            <div class="bar nav quick-section">
                <div class="bar-inner">
                    <ul>
                        <?php
                        global $routesSideBar;
                        foreach ($routesSideBar as $menu):
                            $hasChild = count($menu['children']) > 0 ? true : false;
                            ?>
                            <li class="horizontal">
                                <a href="<?= $hasChild ? 'javascript:;' : URL($menu['slug']) ?>">
                                    <i class="material-icons"><?= isset($menu['icon']) ? $menu['icon'] : '' ?></i>
                                    <span class="title"><?= $menu['name'][\App\Config::LangDefault] ?></span>
                                </a>
                                <?php if ($hasChild): ?>
                                    <ul class="horizontal">
                                        <li><a href="<?= URL($menu['slug']) ?>"> <?= $menu['name'][\App\Config::LangDefault] ?></a></li>
                                        <?php foreach ($menu['children'] as $child): ?>
                                            <?php if (isset($child['menu'])&& $child['menu']=='yes'): ?>
                                                <li><a href="<?= URL($menu['slug'] . '/' . $child['slug']) ?>"> <?= $child['name'][\App\Config::LangDefault] ?></a></li>
                                            <?php endif; ?>

                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>

                    </ul>
                </div>
            </div>

            <div class="pull-right">
                <div class="chat-toggler sm">
                    <div class="profile-pic">
                        <img src="<?=BASE_URL?>/assets/img/profiles/avatar_small.jpg" alt="" data-src="<?=BASE_URL?>/assets/img/profiles/avatar_small.jpg" data-src-retina="<?=BASE_URL?>/assets/img/profiles/avatar_small2x.jpg" width="35" height="35" />
                        <div class="availability-bubble online"></div>
                    </div>
                </div>
                <ul class="nav quick-section ">
                    <li class="quicklinks">
                        <a href="#" class="" id="my-task-list" data-placement="bottom" data-content="" data-toggle="dropdown" data-original-title="Notifications">
                            <i class="material-icons">notifications_none</i>
                            <span class="badge badge-important bubble-only"></span>
                        </a>
                    </li>
                    <li class="quicklinks"> <span class="h-seperate"></span></li>

                    <li class="quicklinks">
                        <a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options">
                            <i class="material-icons">tune</i>
                        </a>
                        <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                            <li>
                                <a href="user-profile.html"> My Account</a>
                            </li>
                            <li>
                                <a href="calender.html">My Calendar</a>
                            </li>
                            <li>
                                <a href="email.html"> My Inbox&nbsp;&nbsp;
                                    <span class="badge badge-important animated bounceIn">2</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout"><i class="material-icons">power_settings_new</i>&nbsp;&nbsp;Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <li class="quicklinks"> <span class="h-seperate"></span></li>
                    <li class="quicklinks">
                        <a href="#" class="chat-menu-toggle" data-webarch="toggle-right-side"><i class="material-icons">chat</i><span class="badge badge-important hide">1</span>
                        </a>
                        <div class="simple-chat-popup chat-menu-toggle hide animated fadeOut">
                            <div class="simple-chat-popup-arrow"></div>
                            <div class="simple-chat-popup-inner">
                                <div style="width:100px">
                                    <div class="semi-bold">David Nester</div>
                                    <div class="message">Hey you there </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div id="notification-list" style="display:none">
                <div style="width:300px">
                    <div class="notification-messages info">
                        <div class="user-profile">
                            <img src="assets/img/profiles/d.jpg" alt="" data-src="assets/img/profiles/d.jpg" data-src-retina="assets/img/profiles/d2x.jpg" width="35" height="35">
                        </div>
                        <div class="message-wrapper">
                            <div class="heading">
                                David Nester - Commented on your wall
                            </div>
                            <div class="description">
                                Meeting postponed to tomorrow
                            </div>
                            <div class="date pull-left">
                                A min ago
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="notification-messages danger">
                        <div class="iconholder">
                            <i class="icon-warning-sign"></i>
                        </div>
                        <div class="message-wrapper">
                            <div class="heading">
                                Server load limited
                            </div>
                            <div class="description">
                                Database server has reached its daily capicity
                            </div>
                            <div class="date pull-left">
                                2 mins ago
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="notification-messages success">
                        <div class="user-profile">
                            <img src="assets/img/profiles/h.jpg" alt="" data-src="assets/img/profiles/h.jpg" data-src-retina="assets/img/profiles/h2x.jpg" width="35" height="35">
                        </div>
                        <div class="message-wrapper">
                            <div class="heading">
                                You haveve got 150 messages
                            </div>
                            <div class="description">
                                150 newly unread messages in your inbox
                            </div>
                            <div class="date pull-left">
                                An hour ago
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <!-- END CHAT TOGGLER -->
        </div>

        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>