<?php $html = new \Core\HTML() ?>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple form-grid">
            <div class="grid-body no-border">
                <form class="form-no-horizontal-spacing form_validation" action="/<?= $slugBase ?>/edit/<?= $dataEdit['_id'] ?>"
                      id="form-data">
                    <input type="hidden" name="action" value="edit">
                    <div class="row column-seperation">
                        <div class="col-md-6">
                            <h4>Thông tin</h4>
                            <div class="row form-row">
                                <div class="col-md-6">
                                    <?php $html->inputText('department_name', 'department_name', 'form-control', $dataEdit['department_name'], '', 'Tên phòng ban'); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php $html->inputText('department_short_name', 'department_short_name', 'form-control', $dataEdit['department_short_name'], '', 'Tên viết tắt'); ?>
                                </div>
                                <div class="col-md-12" id="boxRole">
                                    <fieldset style="padding: 10px;">
                                        <legend>Danh sách chức vụ</legend>
                                        <div id="box-custom-add">
                                         <?php foreach ($dataEdit['department'] as $i => $item){
                                                    echo '<div class="row form-row" id="'.$i.'"><div class="col-md-3"><label>Tên chức vụ</label></div><div class="col-md-7"><input class="form-control"  id="" name="department['.$i.'][name]" type="text" value="'.$item->name.'" placeholder=""></div><div class="col-md-2"> <span class="btn btn-danger btn-sm btn-small " onclick="minusCustomField(\'' .$i. '\')"><i class="fa fa-minus"></i></span></div></div>';
                                                }
                                         ?>
                                        </div>
                                        <div class="text-right">
                                            <button onclick="add_relationship()" type="button" class="btn btn-info btn-small btn-cons btn-material"><i class="material-icons">add_circle_outline</i> <span>Thêm</span></button>
                                        </div>
                                    </fieldset>
                                </div>

                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <textarea name="description" class="form-control" rows="6"
                                              placeholder="Mô tả"><?=$dataEdit['description']?></textarea>
                                </div>
                            </div>

                            <div class="row form-row">
                                <div class="col-md-12">
                                    <label for=""><strong>Trạng thái</strong></label>
                                    <div class="radio radio-primary">
                                        <input id="Disabled" type="radio" name="status"
                                               value="disabled" <?= $dataEdit['status'] == 'disabled' ? 'checked="checked"' : '' ?>>
                                        <label for="Disabled">Disabled</label>
                                        <input id="ADisabled" type="radio" name="status"
                                               value="Enabled" <?= $dataEdit['status'] == 'Enabled' ? 'checked="checked"' : '' ?>>
                                        <label for="ADisabled">Enabled</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <style>
                                .material-icons.role-icon {
                                    font-size: 19px;
                                    department: relative;
                                    top: 3px;
                                    margin-right: 5px;
                                    text-shadow: none;
                                }
                            </style>
                            <h4>Tài nguyên sử dụng</h4>
                            <?php
                            $dataRole = isset($dataEdit['roles'])?$dataEdit['roles']:null;
                            global $routesSideBar;
                            foreach ($routesSideBar as $menu):
                                $hasChild = count($menu['children']) > 0 ? true : false;
                                ?>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend><label>
                                                    <strong>
                                                        <i class="material-icons role-icon"><?= isset($menu['icon']) ? $menu['icon'] : '' ?></i>
                                                        <span class="title"><?= $menu['name'][\App\Config::LangDefault] ?></span>
                                                    </strong>
                                                </label></legend>

                                            <?php if ($hasChild): ?>
                                                <?php foreach ($menu['children'] as $child): ?>
                                                    <?php if (isset($child['roles']) && $child['roles'] == 'yes'):
                                                        $object = $menu['slug'];
                                                        $action = $child['action']['action'];
                                                        $check = isset($dataRole->$object->$action) && $dataRole->$object->$action == 'enable' ? 'checked' : '';

                                                        ?>
                                                        <div class="col-md-3">
                                                            <div class="checkbox check-primary">
                                                                <input class="all_checkbox<?= $menu['slug'] ?>" <?=$check?> name="roles[<?= $menu['slug'] ?>][<?= $child['action']['action'] ?>]"
                                                                       id="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>" type="checkbox" value="enable">
                                                                <label for="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>"> <?= $child['name'][\App\Config::LangDefault] ?></label>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </fieldset>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">

    function minusCustomField(id) {
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }
    var idCustom =1;
    function add_relationship() {
        var id = 'feldID' + idCustom;
        idCustom++;
        var html = '<div class="row form-row" id="'+id+'"><div class="col-md-3"><label>Tên chức vụ</label></div><div class="col-md-7"><input class="form-control" onchange="ChangeToSlug(this.value,\''+id+'_from\')" id="" name="department['+id+'][name]" type="text" value="" placeholder=""></div><div class="col-md-2"> <span class="btn btn-danger btn-sm btn-small " onclick="minusCustomField(\'' + id + '\')"><i class="fa fa-minus"></i></span></div></div>';
        var d1 = document.getElementById('box-custom-add');
        d1.insertAdjacentHTML('beforeend', html);
    }
    $(document).ready(function () {
        //form validation rules

    $("#form-data").validate({
        submitHandler: function (form) {
            var actionUrl = $(form).attr('action');
            $.ajax({
                url: actionUrl,
                type: 'post',
                data: $(form).serializeArray(),
                dataType: 'html',
                beforeSend: function () {
                },
                success: function (data, textStatus, jQxhr) {
                    $('#editDataModal').modal('hide');
                    showMessage('success', '', 'Cập nhật thành công!');
                    _loadHTML('<?=BASE_URL?>/'+$('#page').val())
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });

        }
    });
    });

</script>


