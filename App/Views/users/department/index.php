<!-- BEGIN PAGE CONTAINER-->
<div class="content sm-gutter">
    <div class="page-title">
        <button onclick="addNew('/users/department/add')" type="button" class="btn btn-small btn-primary btn-cons btn-material btn-add-new"><i class="material-icons">add_circle_outline</i> <span>Thêm mới</span></button>
        <h3><?= $pageTitle ?></h3>
    </div>
    <!-- BEGIN DASHBOARD TILES -->
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple ">
                <div class="grid-title no-border text-right">
                    <form action="<?=$slugBase?>" method="get" class="miniForm" id="searchForm">
                        <div class="col-xs-3">
                            <div class="m-r-10 input-prepend inside search-form no-boarder" style="    border: 1px solid #ccc;float: left;width: 100%;border-radius: 5px;">
                                <span class="add-on"> <i class="material-icons">search</i></span>
                                <input  name="search" id="search_Input" type="text" class="no-boarder " value="<?=$search?>" placeholder="Tìm kiếm theo tên hoặc Email" style="width:300px;">
                            </div>
                            <input type="hidden" name="limit" id="hideLimit" value="<?=$limit?>"  >
                            <input type="hidden" name="order_by" id="order_by" value="<?=$order_by?>"  >
                            <input type="hidden" name="order" id="order" value="<?=$order?>"  >
                            <input type="hidden" name="paged" id="page_s" value="<?=$paged?>"  >
                        </div>
                        <div class="col-xs-2">
                            <select name="role" class="form-control">

                                <option value="">---- Phòng ban ----</option>

                            </select>
                        </div>
                        <div class="col-xs-2">
                            <select name="role" class="form-control">
                                <option value="">---- Giới tính ----</option>

                            </select>
                        </div>

                        <div class="col-xs-2">
                            <?php $html->select('status','status','form-control',$status,\App\Config::USER_STATUS,'Trạng thái',true);  ?>
                        </div>
                        <div class="col-xs-3">
                            <button onclick="searchForm()" type="button" class="btn btn-info btn-small btn-cons btn-material"><i class="material-icons">search</i> <span>Tìm kiếm</span></button>

                        </div>
                    </form>
                </div>
                <div class="grid-body no-border">
                    <table class="table table-striped table-flip-scroll cf dataTableSort ">
                        <colgroup>
                            <col style="width:2%">
                            <col style="width:10%">
                            <col style="width:30%">
                            <col style="width:10%">
                            <col style="width:5%">
                            <col style="width:5%">
                        </colgroup>
                        <thead class="cf label-inverse">
                        <tr>
                            <th>
                                <div class="checkbox check-default ">
                                    <input id="checkbox1" type="checkbox" value="1" class="checkall">
                                    <label for="checkbox1"></label>
                                </div>
                            </th>
                            <th><a href="javascript:_sortOrderBy('department_name','<?=$order=="asc"?'desc':'asc'?>')">Thành viên <i class="fa fa-sort"></i></a></th>
                            <th>Mô tả</th>
                            <th><a href="javascript:_sortOrderBy('created_at','<?=$order=="asc"?'desc':'asc'?>')">Ngày tạo <i class="fa fa-sort"></i></a></th>
                            <th>Trạng thái</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $ios=''; foreach ($dataLists as $i => $item):
                            $_id=$item['_id'];
                            $ios .=" 
                                var changeCheckbox$i = document.querySelector('#iosblue$i');
                                var xxx$i = new Switchery(changeCheckbox$i);
                                changeCheckbox$i.onchange = function() {

                                };";
                            ?>
                            <tr id="<?= $item['_id'] ?>">
                                <td>
                                    <div class="checkbox check-default">
                                        <input id="checkbox<?= $item['_id'] ?>" type="checkbox" value="1">
                                        <label for="checkbox<?= $item['_id'] ?>"></label>
                                    </div>
                                </td>

                                <td>
                                    <?= $item['department_name'] ?>
                                </td>
                                <td><?= $item['description'] ?></td>
                                <td><?= date('d/m/Y',$item['created_at']) ?></td>
                                <td>
                                    <div class="slide-primary">
                                        <input type="checkbox" name="switch<?=$i?>" class="ios"  id="iosblue<?=$i?>" <?=$item['status']!='Enabled'?'':'checked'?> />
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-primary btn-xs btn-mini btn-info inline tip" data-toggle="tooltip" title="" data-original-title="Sửa!" onclick="editData('users/department/edit/<?= $item['_id'] ?>','users/department/page/<?=$paged?>')"> <i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-primary btn-xs btn-mini btn-danger inline tip"data-toggle="tooltip" title="" data-original-title="Xóa!" onclick="deleteDataForm('users/department/delete/<?= $item['_id'] ?>','users/department/page/<?=$paged?>')"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="col-md-1">
                        <select class="form-control" id="limit" onchange="limitChange(this.value);">
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                    <div class="col-md-11">
                        <div class="pagination">
                            <?php echo $pagination->toHtml(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD TILES -->

</div>
<!-- END PAGE CONTAINER -->
<script>
    var input = document.getElementById("search_Input");

    // Execute a function when the user releases a key on the keyboard
    input.addEventListener("keyup", function(event) {
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Trigger the button element with a click
            searchForm();
        }
    });
    <?=$ios;?>
</script>
<?php
PT_addJs('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js');
PT_addJs('https://cdnjs.cloudflare.com/ajax/libs/trianglify/0.2.1/trianglify.min.js');
PT_addJs('libs/my/daterangepicker.js');
PT_addJs('libs/my/date-c.js');
PT_addJs('libs/my/switchery.min.js');
PT_addJs('libs/my/script.js');

?>


