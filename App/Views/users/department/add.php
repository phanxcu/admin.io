<?php $html = new \Core\HTML() ?>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple form-grid">
            <div class="grid-body no-border">
                <form class="form-no-horizontal-spacing form_validation" action="/<?= $slugBase ?>/department/add"
                      id="form-data">
                    <input type="hidden" name="action" value="add">
                    <div class="row column-seperation">
                        <div class="col-md-6">
                            <h4>Thông tin</h4>
                            <div class="row form-row">
                                <div class="col-md-6">
                                    <?php $html->inputText('department_name', 'department_name', 'form-control', '', '', 'Tên phòng ban'); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php $html->inputText('department_short_name', 'department_short_name', 'form-control', '', '', 'Tên viết tắt'); ?>
                                </div>
                                <div class="col-md-12" id="boxRole">
                                    <fieldset style="padding: 10px;">
                                        <legend>Danh sách chức vụ</legend>
                                        <div id="box-custom-add"></div>
                                        <div class="text-right">
                                            <button onclick="add_relationship()" type="button"
                                                    class="btn btn-info btn-small btn-cons btn-material"><i
                                                        class="material-icons">add_circle_outline</i> <span>Thêm</span>
                                            </button>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <textarea name="description" class="form-control" rows="6"
                                              placeholder="Mô tả"></textarea>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <label for=""><strong>Trạng thái</strong></label>
                                    <div class="radio radio-primary">
                                        <input id="Disabled" type="radio" name="status" value="disabled">
                                        <label for="Disabled">Disabled</label>
                                        <input id="ADisabled" type="radio" name="status" value="Enabled"
                                               checked="checked">
                                        <label for="ADisabled">Enabled</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <style>
                                .material-icons.role-icon {
                                    font-size: 19px;
                                    department: relative;
                                    top: 3px;
                                    margin-right: 5px;
                                    text-shadow: none;
                                }
                            </style>
                            <h4>Tài nguyên sử dụng</h4>
                            <?php
                            global $routesSideBar;
                            foreach ($routesSideBar as $menu):
                                $hasChild = count($menu['children']) > 0 ? true : false;
                                ?>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend><label>
                                                    <strong>
                                                        <i class="material-icons role-icon"><?= isset($menu['icon']) ? $menu['icon'] : '' ?></i>
                                                        <span class="title"><?= $menu['name'][\App\Config::LangDefault] ?></span>
                                                    </strong>
                                                </label></legend>


                                            <?php if ($hasChild): ?>
                                                <?php foreach ($menu['children'] as $child): ?>
                                                    <?php if (isset($child['roles']) && $child['roles'] == 'yes'): ?>
                                                        <div class="col-md-3">
                                                            <div class="checkbox check-primary">
                                                                <input class="all_checkbox<?= $menu['slug'] ?>"
                                                                       name="roles[<?= $menu['slug'] ?>][<?= $child['action']['action'] ?>]"
                                                                       id="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>"
                                                                       type="checkbox" value="enable">
                                                                <label for="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>"> <?= $child['name'][\App\Config::LangDefault] ?></label>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </fieldset>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    function ChangeToSlug(title, to) {
        var slug;
        //Lấy text từ thẻ input title
        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();
        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "_");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '_');
        slug = slug.replace(/\-\-\-\-/gi, '_');
        slug = slug.replace(/\-\-\-/gi, '_');
        slug = slug.replace(/\-\-/gi, '_');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        //In slug ra textbox có id “slug”
        document.getElementById(to).value = slug;
    }
    function minusCustomField(id) {
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }
    var idCustom = 1;
    function add_relationship() {
        var id = 'feldID' + idCustom;
        idCustom++;
        var html = '<div class="row form-row" id="' + id + '"><div class="col-md-3"><label>Tên chức vụ</label></div><div class="col-md-7"><input class="form-control" onchange="ChangeToSlug(this.value,\'' + id + '_from\')" id="" name="department[' + id + '][name]" type="text" value="" placeholder=""></div><div class="col-md-2"> <span class="btn btn-danger btn-sm btn-small " onclick="minusCustomField(\'' + id + '\')"><i class="fa fa-minus"></i></span></div></div>';
        var d1 = document.getElementById('box-custom-add');
        d1.insertAdjacentHTML('beforeend', html);
    }
    $(document).ready(function () {
        //form validation rules
        $("#form-data").validate({
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Tên không được bỏ trống",
                }
            },
            invalidHandler: function (event, validator) {
                //display error alert on form submit
            },

            errorPlacement: function (label, element) { // render error placement for each input type
                $('<span class="error"></span>').insertAfter(element).append(label)
            },

            highlight: function (element) { // hightlight error inputs

            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {

            },

            submitHandler: function (form) {
                var actionUrl = $(form).attr('action');
                $.ajax({
                    url: actionUrl,
                    type: 'post',
                    data: $(form).serializeArray(),
                    dataType: 'html',
                    success: function (data, textStatus, jQxhr) {
                        $('#addNewModal').modal('hide');
                        showMessage('success', '', 'Tạo thành công!');
                        _loadHTML('/users/department');

                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });

            }
        });


    });

</script>


