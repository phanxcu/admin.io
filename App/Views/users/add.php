<?php $html = new Core\HTML; ?>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple form-grid">
            <div class="grid-body no-border">
                <form class="form-no-horizontal-spacing form_validation" action="/<?= $slugBase ?>/add"
                      id="form-add-user">
                    <input type="hidden" name="action" value="add">
                    <div class="row column-seperation">
                        <div class="col-md12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active">
                                    <a href="#info1" role="tab" data-toggle="tab" aria-expanded="true">Thông tin cơ bản</a>
                                </li>
                                <li class="">
                                    <a href="#info3" role="tab" data-toggle="tab" aria-expanded="false">Thông tin cá nhân</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="info1">
                                    <div class="row column-seperation">
                                        <div class="col-md-6">
                                            <div class="row form-row">
                                                <div class="col-md-6">
                                                    <?php $html->inputText('user_code', 'user_code', 'form-control', '', 'vd: NV001', 'Mã nhân viên'); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php $html->select('status', 'status', 'form-control', '', \App\Config::USER_STATUS, 'Trạng thái'); ?>
                                                </div>
                                            </div>
                                            <div class="row form-row">
                                                <div class="col-md-6">
                                                    <?php $html->inputText('email', 'email', 'form-control', '', 'email@hyh.com.vn', 'Email'); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php $html->inputText('fullName', 'fullName', 'form-control', '', 'Họ Tên', 'Họ tên'); ?>
                                                </div>
                                            </div>
                                            <div class="row form-row">
                                                <div class="col-md-4">
                                                    <?php $html->select('gender', 'gender', 'form-control', '', \App\Config::USER_GENDER, 'Giới tính'); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php $html->inputText('nation', 'nation', 'form-control', '', 'vd: Kinh', 'Dân tộc'); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <?php $html->select('marred', 'marred', 'form-control', '', \App\Config::USER_MARRY, 'Tình trạng hôn nhân'); ?>
                                                </div>
                                            </div>
                                            <div class="row form-row">
                                                <div class="col-md-6">
                                                    <label>Ngày Sinh</label>
                                                    <?php $html->select('birth_date', 'marred', 'form-control w30', '', \App\Config::DATES, 'Ngày', true); ?>
                                                    <?php $html->select('birth_month', 'birth_month', 'form-control  w30', '', \App\Config::MONTHS, 'Tháng', true); ?>
                                                    <?php $html->inputNumber('birth_year', 'birth_year', 'form-control  w30', '', 'Năm', '', 1930, 2019); ?>

                                                </div>
                                                <div class="col-md-6">
                                                    <?php $html->inputText('phone', 'phone', 'form-control', '', 'vd: 0989758995', 'Điện thoại'); ?>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="row form-row">
                                                <div class="col-md-6">
                                                    <?php $html->inputDate('work_try_date', 'official_date', 'form-control', '', 'Ngày thử việc'); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php $html->inputDate('official_date', 'official_date', 'form-control', '', 'Ngày Chính thức'); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php $html->select('marred', 'marred', 'form-control', '', \App\Config::HR_LOCATION, 'Nơi Làm việc'); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php $html->select('marred', 'marred', 'form-control', '', \App\Config::HR_LOCATION, 'Phòng Ban'); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php $html->select('marred', 'marred', 'form-control', '', \App\Config::HR_LOCATION, 'Chức vụ'); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php $html->select('marred', 'marred', 'form-control', '', \App\Config::HR_LOCATION, 'Cấp bậc'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="info3">
                                    <div class="row column-seperation">
                                        <div class="col-md-6">
                                            <fieldset style="padding: 10px;">
                                                <legend>Chứng minh thư</legend>
                                                <div class="row form-row">
                                                    <div class="col-md-6">
                                                        <?php $html->inputText('id_cmt', 'id_cmt', 'form-control', '', 'vd: ', 'Số Chứng minh thư'); ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <?php $html->inputDate('id_create_date', 'id_create_date', 'form-control', '', 'Ngày cấp'); ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <?php $html->inputText('id_create_local', 'id_create_local', 'form-control', '', '', 'Nơi cấp'); ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <?php $html->inputText('o_local', 'id_create_local', 'form-control', '', '', 'Quê quán'); ?>
                                                    </div>

                                                </div>
                                            </fieldset>
                                            <fieldset style="padding: 10px;">
                                                <legend>Hộ khẩu thường trú</legend>
                                                <div class="row form-row">
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('town', 'town', 'form-control', '', '', 'Xã'); ?>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <?php $html->inputText('district', 'district', 'form-control', '', '', 'Quận/Huyện'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('province', 'province', 'form-control', '', '', 'Tỉnh/Thành Phố'); ?>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset style="padding: 10px;">
                                                <legend>Nơi ở hiện tại</legend>
                                                <div class="row form-row">
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('town_live', 'town_live', 'form-control', '', '', 'Xã'); ?>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <?php $html->inputText('district_live', 'district_live', 'form-control', '', '', 'Quận/Huyện'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('province_live', 'province_live', 'form-control', '', '', 'Tỉnh/Thành Phố'); ?>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset style="padding: 10px;">
                                                <legend>Hồ sơ cá nhân</legend>
                                                <div class="row form-row">
                                                    <div class="col-md-4">
                                                        <?php $html->inputCheckBox('user_cv', 'user_cv', 'form-control', '', 'Sơ yếu lý lịch'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputCheckBox('user_cmt_cong_chung', 'user_cmt_cong_chung', 'form-control', '', 'CMT Công chứng'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputCheckBox('so_ho_khau', 'so_ho_khau', 'form-control', '', 'Sổ hộ khẩu công chứng'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputCheckBox('giay_ksk', 'giay_ksk', 'form-control', '', 'Giấy khám sức khỏe'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputCheckBox('bang_tn', 'bang_tn', 'form-control', '', 'Bằng TN'); ?>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row form-row">
                                                <div class="col-md-4">
                                                    <?php $html->inputEmail('email_personal', 'email_personal', 'form-control', '', '', 'Email Cá Nhân'); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <?php $html->inputText('ma_so_thue', 'ma_so_thue', 'form-control', '', '', 'Mã số thuế'); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <?php $html->inputText('so_so_bao_hiem', 'so_so_bao_hiem', 'form-control', '', '', 'Số sổ Bảo Hiểm'); ?>
                                                </div>
                                            </div>
                                            <fieldset style="padding: 10px;">
                                                <legend>Học vấn</legend>
                                                <div class="row form-row">
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('learn_level', 'learn_level', 'form-control', '', '','Trình độ'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('learn_type', 'learn_type', 'form-control', '', '','Chuyên ngành'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('learn_university', 'learn_university', 'form-control', '', '','Trường Đào tạo'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('learn_class', 'learn_class', 'form-control', '', '','Hệ đào tạo'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('graduation_level', 'graduation_level', 'form-control', '', '','Loại Tốt Nghiệp'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('graduation_year', 'graduation_year', 'form-control', '', '','Năm Tốt nghiệp'); ?>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset style="padding: 10px;">
                                                <legend>Người Liên hệ</legend>
                                                <div class="row form-row">
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('contact_name', 'contact_name', 'form-control', '', '','Người khi cần liên hệ'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('contact_phone', 'contact_phone', 'form-control', '', '','Điện thoại người liên hệ'); ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php $html->inputText('contact_address', 'contact_address', 'form-control', '', '','Địa chỉ người liên hệ'); ?>
                                                    </div>

                                                </div>
                                            </fieldset>
                                            <fieldset style="padding: 10px;">
                                                <legend>Gia đình</legend>
                                                <div class="" id="box-custom-field">

                                                </div>
                                                <div class="add_relationship">
                                                    <div class="col-md-12 text-right">
                                                        <button onclick="add_relationship()" type="button" class="btn btn-info btn-small btn-cons btn-material"><i class="material-icons">add_circle_outline</i> <span>Thêm</span></button>
                                                    </div>
                                                </div>
                                            </fieldset>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function minusCustomField(id) {
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }
    var idCustom =1;
    function add_relationship() {
        var id = 'feldID' + idCustom;
        idCustom++;
        var html = '<div class="row form-row" id="'+id+'"><div class="col-md-4"><label>Họ và tên</label><input class="form-control" id="" name="family[][family_name]" type="text" value="" placeholder=""></div><div class="col-md-4"><label>Điện thoại</label><input class="form-control" id="" name="family[family_contact]" type="text" value="" placeholder=""></div><div class="col-md-3"><label>Quan hệ</label><select class="form-control" id="" name="family[family_relationship]"><option value="">Quan hệ</option><option value="father">Bố</option><option value="mother">Mẹ</option><option value="wife">Vợ</option><option value="husband">Chồng</option><option value="sister">Chị/Em</option><option value="brother">Anh/Em</option></select></div><div class="col-md-1"> <label for="">&nbsp;</label><span class="btn btn-danger btn-sm btn-small " onclick="minusCustomField(\'' + id + '\')"><i class="fa fa-minus"></i></span></div></div>';
        var d1 = document.getElementById('box-custom-field');
        d1.insertAdjacentHTML('beforeend', html);
    }

    $(document).ready(function () {
        //Date Pickers
//        $('.input-append.date').datepicker({
//            autoclose: true,
//            todayHighlight: true
//        });

        $(".checkALLRule").change(function () {  //"select all" change
            var status = this.checked; // "select all" checked status
            var id = $(this).attr('id');
            $('.' + id).each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });
        $('#chooseStyleRole').change(function () {
            var style = $(this).val();
            if (style == 'role') {
                $('#roles').prop('disable', false);
                $('#boxCustomRole').hide();
                $('#roles').show();
            } else {
                $('#roles').prop('disable', true);
                $('#boxCustomRole').show();
                $('#roles').hide();
            }
        });

        //form validation rules
        $("#form-add-user").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "users/email",
                        type: "post",
                        data: {
                            email: function () {
                                return $("#email").val();
                            }
                        }
                    },
                }
//                txt_password: {
//                    required: true,
//                    minlength: 8
//                },
//                txt_password_repeat: {
//                    required: true,
//                    equalTo: txt_password,
//                    minlength: 8
//                }
            },
            messages: {
                email: {
                    required: "Địa chỉ email không được bỏ trống.",
                    email: "Nhập đúng định dạng email.",
                    remote: jQuery.validator.format("{0} đã tồn tại.")
                },
                txt_password: {
                    required: "Mật khẩu không được bỏ trống",
                    minlength: "Mật khẩu có ít nhất 8 kí tụ",
                },
                txt_password_repeat: {
                    equalTo: "Mật khẩu không giống nhau!",
                    required: "Mật khẩu không được bỏ trống",

                }
            },
            invalidHandler: function (event, validator) {
                //display error alert on form submit
            },

            errorPlacement: function (label, element) { // render error placement for each input type
                $('<span class="error"></span>').insertAfter(element).append(label)
            },

            highlight: function (element) { // hightlight error inputs

            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {

            },

            submitHandler: function (form) {
                var actionUrl = $(form).attr('action');
                $.ajax({
                    url: actionUrl,
                    type: 'post',
                    data: $(form).serializeArray(),
                    dataType: 'html',
                    success: function (data, textStatus, jQxhr) {
                        $('#addNewModal').modal('hide');
                        showMessage('Thêm thành công!');
                        _loadHTML('/users');

                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });

            }
        });


    });

</script>


