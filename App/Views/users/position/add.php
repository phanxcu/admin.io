<?php $html = new \Core\HTML() ?>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple form-grid">
            <div class="grid-body no-border">
                <form class="form-no-horizontal-spacing form_validation" action="/<?= $slugBase ?>/position/add"
                      id="form-data">
                    <input type="hidden" name="action" value="add">
                    <div class="row column-seperation">
                        <div class="col-md-6">
                            <h4>Thông tin</h4>
                            <div class="row form-row">
                                <div class="col-md-6">
                                    <?php $html->inputText('position_name', 'position_name', 'form-control', '', '', 'Tên Chức danh'); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php $html->inputText('position_short_name', 'position_short_name', 'form-control', '', '', 'Tên viết tắt'); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php $html->select('position_department', 'position_department', 'form-control', '', $dataDepartments, 'Phòng ban'); ?>
                                </div>

                                <div class="col-md-6">
                                    <?php $html->select('position_department_parent', 'position_department_parent', 'form-control', '', $dataPosition, 'Cấp trên'); ?>
                                </div>


                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <?php $html->inputTextArea('position_description', 'position_description', 'form-control', '', '', 'Mô tả'); ?>
                                </div>
                            </div>

                            <div class="row form-row">
                                <div class="col-md-12">
                                    <label for=""><strong>Trạng thái</strong></label>
                                    <div class="radio radio-primary">
                                        <?php $html->inputRadio('status_d', 'status', 'form-control', '', 'Disabled', 'Tắt'); ?>
                                        <?php $html->inputRadio('status_e', 'status', 'form-control','Enabled', 'Enabled', 'Bật'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <style>
                                .material-icons.role-icon {
                                    font-size: 19px;
                                    position: relative;
                                    top: 3px;
                                    margin-right: 5px;
                                    text-shadow: none;
                                }
                            </style>
                            <h4>Phân quyền sử dụng</h4>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <label for=""><strong>Quản lý phòng</strong></label>
                                    <div class="radio radio-primary">

                                        <?php $html->inputRadio('manager_department_d', 'manager_department', 'form-control','', 'Disabled', 'Tắt'); ?>
                                        <?php $html->inputRadio('manager_department_e', 'manager_department', 'form-control','Enabled', 'Enabled', 'Bật'); ?>

                                    </div>
                                </div>
                            </div>
                            <?php
                            global $routesSideBar;
                            foreach ($routesSideBar as $menu):
                                $hasChild = count($menu['children']) > 0 ? true : false;
                                ?>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend><label>
                                                    <strong>
                                                        <i class="material-icons role-icon"><?= isset($menu['icon']) ? $menu['icon'] : '' ?></i>
                                                        <span class="title"><?= $menu['name'][\App\Config::LangDefault] ?></span>
                                                    </strong>
                                                </label></legend>


                                            <?php if ($hasChild): ?>
                                                <?php foreach ($menu['children'] as $child): ?>
                                                    <?php if (isset($child['roles']) && $child['roles'] == 'yes'): ?>
                                                        <div class="col-md-3">
                                                            <div class="checkbox check-primary">
                                                                <input class="all_checkbox<?= $menu['slug'] ?>" name="roles[<?= $menu['slug'] ?>][<?= $child['action']['action'] ?>]"
                                                                       id="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>" type="checkbox" value="enable">
                                                                <label for="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>"> <?= $child['name'][\App\Config::LangDefault] ?></label>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </fieldset>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    function ChangeToSlug(title,to)
    {
        var  slug;

        //Lấy text từ thẻ input title


        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();

        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "_");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '_');
        slug = slug.replace(/\-\-\-\-/gi, '_');
        slug = slug.replace(/\-\-\-/gi, '_');
        slug = slug.replace(/\-\-/gi, '_');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        //In slug ra textbox có id “slug”
        document.getElementById(to).value = slug;
    }
    function minusCustomField(id) {
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }
    var idCustom =1;
    function add_relationship() {
        var id = 'feldID' + idCustom;
        idCustom++;
        var html = '<div class="row form-row" id="'+id+'"><div class="col-md-3"><label>Tên chức vụ</label></div><div class="col-md-7"><input class="form-control" onchange="ChangeToSlug(this.value,\''+id+'_from\')" id="" name="position['+id+'][name]" type="text" value="" placeholder=""></div><div class="col-md-2"> <span class="btn btn-danger btn-sm btn-small " onclick="minusCustomField(\'' + id + '\')"><i class="fa fa-minus"></i></span></div></div>';
        var d1 = document.getElementById('box-custom-add');
        d1.insertAdjacentHTML('beforeend', html);
    }
    $(document).ready(function () {
        //form validation rules
        $("#form-data").validate({
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Tên không được bỏ trống",
                }
            },
            invalidHandler: function (event, validator) {
                //display error alert on form submit
            },

            errorPlacement: function (label, element) { // render error placement for each input type
                $('<span class="error"></span>').insertAfter(element).append(label)
            },

            highlight: function (element) { // hightlight error inputs

            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {

            },

            submitHandler: function (form) {
                var actionUrl = $(form).attr('action');
                $.ajax({
                    url: actionUrl,
                    type: 'post',
                    data: $(form).serializeArray(),
                    dataType: 'html',
                    success: function (data, textStatus, jQxhr) {
                        $('#addNewModal').modal('hide');
                        showMessage('success', '', 'Tạo thành công!');
                        _loadHTML('/users/position');

                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });

            }
        });


    });

</script>


