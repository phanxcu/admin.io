<div class="row">
    <div class="col-md-12">
        <div class="grid simple form-grid">
            <div class="grid-body no-border">
                <form class="form-no-horizontal-spacing form_validation" action="/<?= $slugBase ?>/edit/<?= $dataEdit['_id'] ?>"
                      id="form-data">
                    <input type="hidden" name="action" value="edit">
                    <div class="row column-seperation">
                        <div class="col-md-6">
                            <h4>Thông tin</h4>
                            <div class="row form-row">
                                <div class="col-md-6">
                                    <?php $html->inputText('position_name', 'position_name', 'form-control', $dataEdit['position_name'], '', 'Tên Chức danh'); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php $html->inputText('position_short_name', 'position_short_name', 'form-control',  $dataEdit['position_short_name'], '', 'Tên viết tắt'); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php $html->select('position_department', 'position_department', 'form-control',  $dataEdit['position_department'], $dataDepartments, 'Phòng ban'); ?>
                                </div>

                                <div class="col-md-6">
                                    <?php $html->select('position_department_parent', 'position_department_parent', 'form-control',  $dataEdit['position_department_parent'], $dataPosition, 'Cấp trên'); ?>
                                </div>

                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <?php $html->inputTextArea('position_description', 'position_description', 'form-control', $dataEdit['position_description'], '', 'Mô tả'); ?>
                                </div>
                            </div>

                            <div class="row form-row">
                                <div class="col-md-12">
                                    <label for=""><strong>Trạng thái</strong></label>
                                    <div class="radio radio-primary">
                                        <?php $html->inputRadio('status_d', 'status', 'form-control',  'Disabled',$dataEdit['status'], 'Tắt'); ?>
                                        <?php $html->inputRadio('status_e', 'status', 'form-control', 'Enabled', $dataEdit['status'], 'Bật'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <style>
                                .material-icons.role-icon {
                                    font-size: 19px;
                                    position: relative;
                                    top: 3px;
                                    margin-right: 5px;
                                    text-shadow: none;
                                }
                            </style>
                            <h4>Tài nguyên sử dụng</h4>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <label for=""><strong>Quản lý phòng</strong></label>
                                    <div class="radio radio-primary">
                                        <?php $html->inputRadio('manager_status_d', 'manager_department', 'form-control',  'Disabled',$dataEdit['manager_department'], 'Tắt'); ?>
                                        <?php $html->inputRadio('manager_status_d', 'manager_department', 'form-control', 'Enabled',  $dataEdit['manager_department'],'Bật'); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $dataRole = isset($dataEdit['roles'])?$dataEdit['roles']:null;
                            global $routesSideBar;
                            foreach ($routesSideBar as $menu):
                                $hasChild = count($menu['children']) > 0 ? true : false;
                                ?>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend><label>
                                                    <strong>
                                                        <i class="material-icons role-icon"><?= isset($menu['icon']) ? $menu['icon'] : '' ?></i>
                                                        <span class="title"><?= $menu['name'][\App\Config::LangDefault] ?></span>
                                                    </strong>
                                                </label></legend>

                                            <?php if ($hasChild): ?>
                                                <?php foreach ($menu['children'] as $child): ?>
                                                    <?php if (isset($child['roles']) && $child['roles'] == 'yes'):
                                                        $object = $menu['slug'];
                                                        $action = $child['action']['action'];
                                                        $check = isset($dataRole->$object->$action) && $dataRole->$object->$action == 'enable' ? 'checked' : '';

                                                        ?>
                                                        <div class="col-md-3">
                                                            <div class="checkbox check-primary">
                                                                <input class="all_checkbox<?= $menu['slug'] ?>" <?=$check?> name="roles[<?= $menu['slug'] ?>][<?= $child['action']['action'] ?>]"
                                                                       id="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>" type="checkbox" value="enable">
                                                                <label for="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>"> <?= $child['name'][\App\Config::LangDefault] ?></label>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </fieldset>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">

    function minusCustomField(id) {
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }
    var idCustom =1;
    function add_relationship() {
        var id = 'feldID' + idCustom;
        idCustom++;
        var html = '<div class="row form-row" id="'+id+'"><div class="col-md-3"><label>Tên chức vụ</label></div><div class="col-md-7"><input class="form-control" onchange="ChangeToSlug(this.value,\''+id+'_from\')" id="" name="position['+id+'][name]" type="text" value="" placeholder=""></div><div class="col-md-2"> <span class="btn btn-danger btn-sm btn-small " onclick="minusCustomField(\'' + id + '\')"><i class="fa fa-minus"></i></span></div></div>';
        var d1 = document.getElementById('box-custom-add');
        d1.insertAdjacentHTML('beforeend', html);
    }
    $(document).ready(function () {
        //form validation rules

    $("#form-data").validate({
        submitHandler: function (form) {
            var actionUrl = $(form).attr('action');
            $.ajax({
                url: actionUrl,
                type: 'post',
                data: $(form).serializeArray(),
                dataType: 'html',
                beforeSend: function () {
                },
                success: function (data, textStatus, jQxhr) {
                    $('#editDataModal').modal('hide');
                    showMessage('success', '', 'Cập nhật thành công!');
                    _loadHTML('<?=BASE_URL?>/'+$('#page').val())
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });

        }
    });
    });

</script>


