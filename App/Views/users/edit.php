<div class="row">
    <div class="col-md-12">
        <div class="grid simple form-grid">
            <div class="grid-body no-border">
                <form class="form-no-horizontal-spacing form_validation"
                      action="/<?= $slugBase ?>/update/<?= $dataEdit['_id'] ?>" id="form-condensed">
                    <div class="row column-seperation">
                        <div class="col-md-5">
                            <h4>Thông tin cá nhân</h4>
                            <div class="row form-row">
                                <div class="col-md-6">
                                    <label class="form-label">Email</label>
                                    <input name="email" id="email" type="text" class="form-control" placeholder="Email"
                                           value="<?= $dataEdit['email'] ?>" readonly>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-label">Họ Tên</label>
                                    <input name="firstName" id="form3FirstName" type="text" class="form-control"
                                           value="<?= $dataEdit['fullName'] ?>" placeholder="">
                                </div>
                            </div>

                            <div class="row form-row">
                                <div class="col-md-6">
                                    <label class="form-label">Giới tính</label>
                                    <select name="gender" id="form3Gender" class=" form-control">
                                        <option <?= $dataEdit['gender'] == '1' ? 'selected' : '' ?> value="1">Nam
                                        </option>
                                        <option <?= $dataEdit['gender'] == '2' ? 'selected' : '' ?> value="2">Nữ
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-label">Ngày sinh</label>
                                    <input type="text" placeholder="Date of Birth" class="form-control" id="birth-day"
                                           name="birthday" value="<?= $dataEdit['birthday'] ?>">
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-6">
                                    <label class="form-label">Địa chỉ</label>
                                    <input name="address" id="address" type="text" class="form-control"
                                           placeholder="Address" value="<?= $dataEdit['address'] ?>">
                                </div>
                                <div class="col-md-6">
                                    <label class="form-label">Điện thoại</label>
                                    <input name="phone" id="phone" type="text" class="form-control" placeholder="Phone"
                                           value="<?= $dataEdit['phone'] ?>">
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <textarea name="description" class="form-control" rows="6"
                                              placeholder="Mô tả"><?= $dataEdit['description'] ?></textarea>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <label class="form-label"><strong>Trạng thái</strong></label>
                                    <div class="radio radio-primary">
                                        <div class="radio radio-primary">
                                            <input id="Disabled" type="radio" name="status"
                                                   value="disabled" <?= $dataEdit['status'] == 'disabled' ? 'checked="checked"' : '' ?>>
                                            <label for="Disabled">Disabled</label>
                                            <input id="ADisabled" type="radio" name="status"
                                                   value="Enabled" <?= $dataEdit['status'] == 'Enabled' ? 'checked="checked"' : '' ?>>
                                            <label for="ADisabled">Enabled</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <h4>Vai trò</h4>
                            <div class="row form-row">
                                <div class="col-md-6">
                                    <label class="form-label">Tùy chọn phân quyền</label>
                                    <select name="roleType" id="chooseStyleRole" class=" form-control">
                                        <option <?= $dataEdit['roleType'] == 'roles' ? 'selected' : '' ?> value="role">
                                            Phân quyền
                                        </option>
                                        <option <?= $dataEdit['roleType'] == 'custom' ? 'selected' : '' ?>
                                                value="custom">Tùy chọn
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-6" id="boxRole"
                                     style="<?= $dataEdit['roleType'] == 'roles' ? 'display: block' : 'display: none' ?>">
                                    <label class="form-label">Chọn quyền</label>
                                    <select name="role" id="roles" class=" form-control">
                                        <?php foreach ($dataRoles as $role): ?>
                                            <option <?= isset($dataEdit['roles']) && $dataEdit['roles'] == $role['_id'] ? 'selected' : '' ?>
                                                    value="<?= $role['_id'] ?>"><?= $role['name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                            </div>
                            <div id="boxCustomRole" class="row form-row"
                                 style="<?= $dataEdit['roleType'] == 'custom' ? 'display: block' : 'display: none' ?>">
                                <div class="col-md-12">
                                    <?php
                                    $dataRole = $dataEdit['roles'];
                                    global $routesSideBar;
                                    foreach ($routesSideBar as $menu):

                                        $hasChild = count($menu['children']) > 0 ? true : false;
                                        ?>

                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend><label>
                                                            <strong>
                                                                <i class="material-icons role-icon"><?= isset($menu['icon']) ? $menu['icon'] : '' ?></i>
                                                                <span class="title"><?= $menu['name'][\App\Config::LangDefault] ?></span>
                                                            </strong>
                                                        </label></legend>
                                                    <div class=" form-row">
                                                        <div class="col-md-3">
                                                            <div class="checkbox check-primary">
                                                                <input id="all_checkbox<?= $menu['slug'] ?>"
                                                                       type="checkbox" class="checkALLRule" value="1">
                                                                <label for="all_checkbox<?= $menu['slug'] ?>"
                                                                       style="font-weight: bold;font-size: 15px"> Tất
                                                                    cả</label>
                                                            </div>
                                                        </div>
                                                        <?php if ($hasChild):$checkAll = true; ?>
                                                            <?php foreach ($menu['children'] as $child): ?>
                                                            <?php
                                                        if (isset($child['roles']) && $child['roles'] == 'yes'):
                                                            $object = $menu['slug'];
                                                            $action = $child['action']['action'];
                                                            $check = isset($dataEdit['role']->$object->$action) && $dataEdit['role']->$object->$action == 'enable' ? 'checked' : '';
                                                            ?>
                                                            <div class="col-md-3">
                                                                <div class="checkbox check-primary">
                                                                    <input class="all_checkbox<?= $menu['slug'] ?>"
                                                                           name="role[<?= $menu['slug'] ?>][<?= $child['action']['action'] ?>]"
                                                                        <?php if (isset($dataRole->$parrent->$node) && $dataRole->$parrent->$node == 'enable') echo "checked='checked'"; else $checkAll = false; ?>
                                                                           id="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>"
                                                                           type="checkbox" value="enable">
                                                                    <label for="checkbox<?= $menu['slug'] . '/' . $child['slug'] ?>"> <?= $child['name'][\App\Config::LangDefault] ?></label>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php endforeach; ?>
                                                        <?php if ($checkAll): ?>
                                                            <script>
                                                                $("#all_checkbox<?= $menu['slug'] ?>").prop("checked", true);
                                                            </script>
                                                        <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".checkALLRule").change(function () {  //"select all" change
            console.log(this);
            var status = this.checked; // "select all" checked status
            var id = $(this).attr('id');
            $('.' + id).each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });
        $('#chooseStyleRole').change(function () {
            var style = $(this).val();
            if (style == 'role') {
                $('#roles').prop('disable', false);
                $('#boxCustomRole').hide();
                $('#roles').show();
            } else {
                $('#roles').prop('disable', true);
                $('#boxCustomRole').show();
                $('#roles').hide();

            }
        });

        $(function ($) {
            $("#birth-day").mask("99/99/9999");
        });
        //form validation rules
        $("#form-condensed").validate({
            invalidHandler: function (event, validator) {
                //display error alert on form submit
            },

            errorPlacement: function (label, element) { // render error placement for each input type
                $('<span class="error"></span>').insertAfter(element).append(label)
            },

            highlight: function (element) { // hightlight error inputs

            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {

            },

            submitHandler: function (form) {
                var actionUrl = $(form).attr('action');
                $.ajax({
                    url: actionUrl,
                    type: 'post',
                    data: $(form).serializeArray(),
                    dataType: 'html',
                    success: function (data, textStatus, jQxhr) {
                        // $('#addNewModal').modal('hide');
                        showSuccessMessage('Thêm thành công!');
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });

            }
        });


    });

</script>


