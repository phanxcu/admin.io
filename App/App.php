<?php
/**
 * Created by PhpStorm.
 * User: cu
 * Date: 8/9/18
 * Time: 1:13 AM
 */

namespace App;

use \Core\Router;
use App\Models\Setting;

class App
{
    public function run()
    {
        /**
         * global
         */
        global $post_type;
        $post_type = ['post', 'page'];
        /**
         * Routing
         */
        $router = new Router();
        /**
         * ---------------------------
         * route extension
         * ---------------------------
         */
        global $routesSideBar;
        $routesSideBar = require_once 'route.php';
        /**
         * ---------------------------
         * Add Route Extensions
         * ---------------------------
         */
        $setting = new Setting();

        $dataExtension = $setting->valueSetting('extensions');
        if (is_array($dataExtension)):
            foreach ($dataExtension as $nameExtension => $statusExtension) {
                $fileExtension = dir_extensions . DIRECTORY_SEPARATOR . $nameExtension . DIRECTORY_SEPARATOR  . "init.php";
                if ($statusExtension == 'active') { //check extension is active
                    if (file_exists($fileExtension)) {
                        $dataConfigExtension = require $fileExtension;
                        $routesSideBar [] = $dataConfigExtension['route'];
                    }
                }
            }
        endif;
        foreach ($routesSideBar as $route) {
            $hasChild = count($route['children']) > 0 ? true : false;
            $router->add($route['slug'], ['controller' => $route['action']['controller'], 'action' => $route['action']['action'], 'title' => $route['name']['vn'],'roles' => 'yes']);
            $router->add($route['slug'] . '/page/{page:[0-9-]+}', ['controller' => $route['action']['controller'], 'action' => $route['action']['action'], 'title' => $route['name']['vn']]);
            if ($hasChild):
                foreach ($route['children'] as $child):
                    $router->add(
                        $route['slug'] . '/' . $child['slug'],
                        [
                            'controller' => $child['action']['controller'],
                            'action' => $child['action']['action'],
                            'title' => $child['name']['vn'],
                            'roles' => $child['roles']
                        ]
                    );
                endforeach;
            endif;

        }
        /**
         * ---------------------------
         * Authentication
         * ---------------------------
         */
        $router->add('', ['controller' => 'PageController', 'action' => 'home']);
        $router->add('login', ['controller' => 'LoginController', 'action' => 'login']);
        $router->add('logout', ['controller' => 'LoginController', 'action' => 'logout']);

        /**
         * ---------------------------
         * Admin
         * ---------------------------
         */
//        $router->add('users', ['controller' => 'UserController', 'action' => 'all']);
//        $router->add('users/add', ['controller' => 'UserController', 'action' => 'add']);
//        $router->add('users/save', ['controller' => 'UserController', 'action' => 'save']);
//        $router->add('users/edit/{id:[a-z0-9-]+}', ['controller' => 'UserController', 'action' => 'edit']);
//        $router->add('users/update/{id:[a-z0-9-]+}', ['controller' => 'UserController', 'action' => 'update']);
//        $router->add('users/email', ['controller' => 'UserController', 'action' => 'checkEmailUnique']);
        /**
         * ---------------------------
         * Setting
         * ---------------------------
         */

        /**
         * ---------------------------
         * DataForm
         * ---------------------------
         * //         */
//        $router->add('form-data', ['controller' => 'FormDataController', 'action' => 'index']);
//        $router->add('ajax-data', ['controller' => 'FormDataController', 'action' => 'ajax']);
//        $router->add('form-data/page/{page:[0-9-]+}', ['controller' => 'FormDataController', 'action' => 'index']);

//
//        $lang = \App\Config::multiLang ?'{lang:[a-z-]+}/':'';
//        $router->add('', ['controller' => 'Page', 'action' => 'home']);
//        $router->add('404', ['controller' => 'Page', 'action' => 'pageNotFound']);
//        $router->add('{slug:[a-z0-9-]+}.html', ['controller' => 'Post', 'action' => 'index']);
//        $router->add('{slug:[a-z0-9-]+}', ['controller' => 'Page', 'action' => 'page']); // Xử lý Với Category và Page (Liên hệ, giới thiệu.)
//        $router->add('{slug:[a-z0-9-]+}/', ['controller' => 'Page', 'action' => 'page']); // Xử lý Với Category và Page (Liên hệ, giới thiệu.)
//        $router->add('customer', ['controller' => 'Customer', 'action' => 'profile']); // Xử lý Với Category và Page (Liên hệ, giới thiệu.)
//
//        $router->add('customer/update', ['controller' => 'Customer', 'action' => 'update']); // Xử lý Với Category và Page (Liên hệ, giới thiệu.)
//
//        /**
//         * ---------------------------
//         * Add the routes with language
//         * ---------------------------
//         */
//        $router->add($lang.'{slug:[a-z0-9-]+}.html', ['controller' => 'Post', 'action' => 'index']);
//        $router->add($lang.'{slug:[a-z0-9-]+}', ['controller' => 'Page', 'action' => 'page']); // Xử lý Với Category và Page (Liên hệ, giới thiệu.)
//        $router->add($lang.'customer', ['controller' => 'Customer', 'action' => 'profile']); // Xử lý Với Category và Page (Liên hệ, giới thiệu.)
//        $router->add($lang.'customer/update', ['controller' => 'Customer', 'action' => 'update']); // Xử lý Với Category và Page (Liên hệ, giới thiệu.)
        $query = $_SERVER['QUERY_STRING'];
        $query = (substr($query, -1) == '/') ? substr($query, 0, -1) : $query;
//            substr($string, 0, -1)
        $router->dispatch($query);

    }

}
