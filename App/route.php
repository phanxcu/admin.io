<?php
return array(

    array(
        "slug" => 'users',
        "icon" => 'people',
        "action" => ['controller' => 'UserController', 'action' => 'index'],
        'name' => array('en'=>'User','vn'=>'Quản lý nhân viên'),
        'children' => [
            array("slug" => 'add', "action" => ['controller' => 'UserController', 'action' => 'add'], 'name' => array('en'=>'Add Users','vn'=>'Tạo mới'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'email', "action" => ['controller' => 'UserController', 'action' => 'checkEmailUnique'], 'name' =>array('en'=>'Add Users','vn'=>''), 'menu' => 'no','roles'=>'no'),
            array("slug" => 'reset-password', "action" => ['controller' => 'UserController', 'action' => 'resetPassword'], 'name' =>array('en'=>'Add Users','vn'=>'Đổi mật khẩu'), 'menu' => 'no','roles'=>'no'),
            array("slug" => 'edit/{id:[a-z0-9-]+}', "action" => ['controller' => 'UserController', 'action' => 'edit'], 'name' =>array('en'=>'Edit Info','vn'=>'Cập nhật'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'delete/{id:[a-z0-9-]+}', "action" => ['controller' => 'UserController', 'action' => 'delete'], 'name' =>array('en'=>'Add Users','vn'=>'Xóa'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'trash', "action" => ['controller' => 'UserController', 'action' => 'all'], 'name' => array('en'=>'Add Users','vn'=>'Thùng rác'), 'menu' => 'no','roles'=>'yes'),

            array("slug" => 'department', "action" => ['controller' => 'UserController', 'action' => 'department'], 'name' => array('en'=>'Departments','vn'=>'Quản lý Phòng Ban'), 'menu' => 'yes','roles'=>'yes'),
            array("slug" => 'department/page/{page:[a-z0-9-]+}', "action" => ['controller' => 'UserController', 'action' => 'department'], 'name' => array('en'=>'Departments','vn'=>'Quản lý Phòng Ban'), 'menu' => 'no','roles'=>'no'),
            array("slug" => 'department/add', "action" => ['controller' => 'UserController', 'action' => 'departmentAdd'], 'name' => array('en'=>'Departments','vn'=>'Tạo Phòng Ban'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'department/edit/{id:[a-z0-9-]+}', "action" => ['controller' => 'UserController', 'action' => 'departmentEdit'], 'name' => array('en'=>'Departments','vn'=>'Cập nhật Phòng Ban'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'department/delete/{id:[a-z0-9-]+}', "action" => ['controller' => 'UserController', 'action' => 'departmentDelete'], 'name' => array('en'=>'Departments','vn'=>'Xóa'), 'menu' => 'no','roles'=>'yes'),


            array("slug" => 'position', "action" => ['controller' => 'UserController', 'action' => 'position'], 'name' => array('en'=>'Departments','vn'=>'Quản lý Chức Vụ'), 'menu' => 'yes','roles'=>'yes'),
            array("slug" => 'position/page/{page:[a-z0-9-]+}', "action" => ['controller' => 'UserController', 'action' => 'position'], 'name' => array('en'=>'Departments','vn'=>'Quản lý Chức Vụ'), 'menu' => 'no','roles'=>'no'),
            array("slug" => 'position/add', "action" => ['controller' => 'UserController', 'action' => 'positionAdd'], 'name' => array('en'=>'Departments','vn'=>'Tạo Chức Vụ'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'position/edit/{id:[a-z0-9-]+}', "action" => ['controller' => 'UserController', 'action' => 'positionEdit'], 'name' => array('en'=>'Departments','vn'=>'Cập nhật Chức Vụ'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'position/delete/{id:[a-z0-9-]+}', "action" => ['controller' => 'UserController', 'action' => 'positionDelete'], 'name' => array('en'=>'Departments','vn'=>'Xóa'), 'menu' => 'no','roles'=>'yes'),
        ],
    ),
    array(
        "slug" => 'roles',
        "icon" => 'accessibility_new',
        "action" => ['controller' => 'RoleController', 'action' => 'index'],
        'name' => array('en'=>'Role','vn'=>'Phân quyền'),
        'children' => [
//            array("slug" => 'index/page/{page:[0-9-]+}', "action" => ['controller' => 'RoleController', 'action' => 'index'], 'name' => array('en'=>'Add Role','vn'=>'Vai trò'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'add', "action" => ['controller' => 'RoleController', 'action' => 'add'], 'name' => array('en'=>'Add Role','vn'=>'Tạo Quyền'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'edit/{id:[a-z0-9-]+}', "action" => ['controller' => 'RoleController', 'action' => 'edit'], 'name' =>array('en'=>'Edit Role','vn'=>'Cập nhật'), 'menu' => 'no','roles'=>'yes'),
            array("slug" => 'delete/{id:[a-z0-9-]+}', "action" => ['controller' => 'RoleController', 'action' => 'delete'], 'name' =>array('en'=>'Add Role','vn'=>'Xóa'), 'menu' => 'no','roles'=>'yes'),
        ],
    ),
    array(
        "slug" => 'settings',
        "icon" => 'settings',
        "action" => ['controller' => 'SettingController', 'action' => 'all'],
        'name' => array('en'=>'Settings','vn'=>'Cài đặt'),
        'children' => [
            array("slug" => 'general', "action" => ['controller' => 'SettingController', 'action' => 'general'], 'name' => array('en'=>'Settings','vn'=>'Cài đặt'), 'menu' => 'yes','roles'=>'yes'),
            array("slug" => 'extensions', "action" => ['controller' => 'SettingController', 'action' => 'extensions'], 'name' => array('en'=>'Extensions','vn'=>'Phần mở rộng'), 'menu' => 'yes','roles'=>'yes'),
            array("slug" => 'extensions/update', "action" => ['controller' => 'SettingController', 'action' => 'extensionsUpdate'], 'name'=>array('en'=>'','vn'=>''), 'menu' => 'no','roles'=>'no'),
        ],
    )

);