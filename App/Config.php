<?php

namespace App;

class Config
{
    const BaseUrl = "http://admin.io";
    /**
     * Database host
     * @var string
     */

    const DB_HOST = '45.76.191.149';
    const DB_PORT = '27017';
    const DB_NAME = 'db_ecommerces';
    const DB_USER = 'acc_ecommerces';
    const DB_PASSWORD = 'dbphanCH@123';
    const DB_PREFIX = 'az_';
//
    const DB_DEBUG = TRUE;
    const DB_RETURN = 'array';
    const DB_READ_Preference = 'primary ';
    const DB_READ_Concern = 'majority ';
    const SHOW_ERRORS = true;
    /**
     * Set Template default
     * @var string
     */
    const Template_folder = "admin";
    /**
     * Set Languages
     * @var string
     */
    const multiLang = true;
    const Lang = ['vn', 'en', 'fr'];
    const LangDefault = 'vn';
    const LIMIT = 10;
    const TIME_ZONE = 'Asia/Ho_Chi_Minh';
    const SEND_GRID_API_KEY = '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';
    const EXTENSION_DIR = '\Extensions';
    // Setting HR
    const HR_LOCATION = array(
        'Local_TM' => 'TÂM MỸ',
        'Local_TKC' => 'TRẦN KHÁT CHÂN',
        'Local_TDH' => 'TRẦN DUY HƯNG',
        'Local_LOTTE' => 'LOTTE',
        'Local_HCM' => 'HỒ CHÍ MINH',
        'Local_HP' => 'HẢI PHÒNG',
    );
    const USER_STATUS = array(
        'quit_work' => 'Nghỉ Việc',
        'maternity_leave' => 'Nghỉ Đẻ',
        'part_time' => 'Part Time',
        'internship' => 'Thực tập',
        'official' => 'Chính thức'
    );
    const USER_GENDER = array(
        'males' => 'Nam',
        'females' => 'Nữ',
    );
    const USER_MARRY = array(
        'single' => 'Độc thân',
        'marred' => 'Có gia đình',
    );

    const MONTHS = array(
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        61 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
    );
    const FAMILY_RELATIONSHIP = array(
        'father' => 'Bố',
        'mother' => 'Mẹ',
        'wife' => 'Vợ',
        'husband' => 'Chồng',
        'sister' => 'Chị/Em',
        'brother' => 'Anh/Em',
    );
    const DATES = array(
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12',
        '13',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19',
        '20',
        '21',
        '22',
        '23',
        '24',
        '25',
        '26',
        '27',
        '28',
        '29',
        '30',
        '31',
    );



}
